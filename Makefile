# Configurations [update this for each project]
LOG_DIR=./storage/logs
BACKUP_DIR=./storage/backups
PROJECT_WEB_ROOT=./web# could be: ./
DATE=$(shell date +'%m-%Y')
GIT_MASTER_BRANCH=master# could be: main
GIT_STAGING_BRANCH=staging# if you have more staging branches that you don't want to delete even if thats merged to master, please edit the local-clean-merged-branches task
NODE_VERSION=12.14.1 # keep this same as your project node version
ERROR_LOG_KEYWORDS=web.ERROR queue.ERROR console.ERROR#separate multiple keywords with space
YUMP_CACHE_DIR=./web/yump/private/cache/data

# For printing colored text
ERROR_COLOR := \033[0;31m # Red
WARNING_COLOR := \033[1;33m # Yellow
NORMAL_COLOR := \033[0m # No Color


# Usage make commands:
# TO USE below commands, you can run `make [command]` in your terminal at the same folder where this Makefile is located.
# e.g. `make prod-maintenance-backup`
# 
# Maintenance process:
# 1. [prod-maintenance-backup] Back up production
# In this step, we will backup and download resources from production server.
# Including tasks:
# - [prod-backup-logs] Backup logs to a zip in ./storage/backups (./storage/backups/logs_$(DATE).tar.gz)
# - [prod-backup-uploads] Backup uploads to a zip for download (./storage/backups/uploads_$(DATE).tar.gz
# - [prod-backup-db] Backup database to for download (./storage/backups/CRAFT_CMS_DB_FILE_FORMAT.sql.zip)
#
# 2. [Manual] download the zip files from production server to your local project
# Download uploads: ./storage/backups/uploads_$(DATE).tar.gz
# Download database: ./storage/backups/CRAFT_CMS_DB_FILE_FORMAT.sql.zip
# 
# After downloading the files, move both of them to ./storage/backups folder
#
# 3.[local-maintenance] Sync local to production
# Including tasks:
# - [local-check-node-version] Check to the project node version, it will prompt a message ask you to switch
# - [local-git-sync] Pull checkout to master, pull latest changes
# - [local-sync-uploads] Extract the uploads we download from server which you already moved to your ./storage/backups folder
# - [local-start-ddev] Start ddev
# - [local-composer-install] Run composer install, to make your composer dependencies up to date
# - [local-db-restore] Restore the database we download from server (move the downloads in first step to your local project root folder)
# - [local-clear-logs] Clear the logs in your local first before we start maintenenace
# - [local-switch-maintenance-branch] Create and switch to maintenance branch named maintenance/$(DATE) e.g. maintenance/01-2022
# - [local-clear-craft-caches] Clear the craft caches
# - [local-clear-yump-caches] Clear the yump caches (like menu cache, news cache, etc.)
# 
# 4. [Manual] Do your things, update the craft, test, etc.
# Including tasks:
# - Update the craft cms
# - Quick test to make sure everything generally works
# - If you made style changes and you are using gulp, make sure you update the css or js version number, if using vite then its automatically handled
#
# 5. [local-prepare-staging-deploy] Prepare deploy to staging on local
# Including tasks:
# - [local-check-error-log] Check if there are any error logs in LOG_DIR, if there are propmpt a message and stop the process
# - [local-npm-run-build] Run npm run build to generate a fresh css build
# - [local-git-add-commit] Add everything to git and commit
# - [local-git-push-maintenance] Push the changes to the maintenance branch [Manually type the commit message]
#
# 6. [Manual] Test on staging server, and finish the maintenance report
# Including tasks:
# - Click on the pull request link, create merge request to staging branch
# - Merge the changes to staging branch
# - Install updates on staging server
# - Test the functionalities and blocks
# - Check cpanel usage
# - Update maintenance report and commit your changes to the maintenance branch
# - Push the changes to the maintenance branch
# - Merge changes to master
# 
# Some utility tasks that you can use:
# - [check-log-file] Check if there are any error logs in LOG_DIR, if there are propmpt a message and stop the process
# 
# 7. [Manual] Deploy on production
# In this step, we will do following tasks:
# - Run composer install at the sametime, archive the vendor folder to a zip
#
# Some utility tasks that you can use:
# - [prod-composer-install] Run composer install and create a backup of vendor at the sametime
# 
# 8. [Optional] Clean up local, and some utility classes
# In this step, we will do following tasks:
# - Update your local master branch
# - Clean up zip files created in above tasks
# - Clean up merged branches
# 
# Some utility tasks that you use:
# - [local-clean-merged-branches] Switching to master, pull the lastest change, clean up merged branches
# - [local-clean-zip-files] Clean up zip files created in above tasks

############################################################################################################
# 1. Back up production
# clear the logs and create a backup of logs at "./storage/backups" folder
prod-backup-logs: 
	@echo "Creating a backup of logs..."
	@tar -czf log_$(DATE).tar.gz -C $(LOG_DIR) .
	@echo "Backup created: log_$(DATE).tar.gz"
	@mv log_$(DATE).tar.gz $(BACKUP_DIR)
	@echo "Backup moved to: $(BACKUP_DIR)/log_$(DATE).tar.gz"
	@rm -rf $(LOG_DIR)/*
	@echo "Logs in folder are cleared."
.PHONY: prod-backup-logs

# Backup uploads folder at "./storage/backups" folder
prod-backup-uploads:
	@echo "Creating a backup of uploads..."
	@tar -czf uploads_$(DATE).tar.gz -C ${PROJECT_WEB_ROOT} uploads
	@echo "Backup created: $(BACKUP_DIR)/uploads_$(DATE).tar.gz"
	@mv uploads_$(DATE).tar.gz $(BACKUP_DIR)
	@echo "Backup moved to: $(BACKUP_DIR)/uploads_$(DATE).tar.gz"
	@echo "Uploads backup created."
.PHONY: prod-backup-uploads

# Backup database at "./storage/backups" folder
prod-backup-db:
	@echo "Creating a backup of database..."
	@php craft db/backup --zip
	@echo "Backup created."
.PHONY: prod-backup-db

prod-maintenance-backup:
	@echo "Starting maintenance process..."
	@make prod-backup-logs
	@make prod-backup-uploads
	@make prod-backup-db
	@echo "Maintenance process completed."
.PHONY: prod-maintenance-backup

############################################################################################################
# 3. Sync local to production
# Sync local to production, and prepare local
local-check-node-version:
	@echo "Checking current Node.js version..."
	@CURRENT_VERSION=$$(node -v | tr -d '[:space:]'); \
	TRIMMED_NODE_VERSION=$$(echo $(NODE_VERSION) | sed 's/^ *//;s/ *$$//'); \
	TARGET_VERSION="v$$TRIMMED_NODE_VERSION"; \
	if [ "$$CURRENT_VERSION" != "$$TARGET_VERSION" ]; then \
		printf "WARNING: Current Node.js version is $(WARNING_COLOR)%s$(NORMAL_COLOR). Please switch to $(WARNING_COLOR)%s$(NORMAL_COLOR) by running:\n" "$$CURRENT_VERSION" "$$TRIMMED_NODE_VERSION"; \
		printf "$(WARNING_COLOR)nvm use %s$(NORMAL_COLOR)\n" "$$TRIMMED_NODE_VERSION"; \
		printf "Debug: CURRENT_VERSION='%s'\n" "$$CURRENT_VERSION"; \
		printf "Debug: TARGET_VERSION='%s'\n" "$$TARGET_VERSION"; \
	else \
		echo "Node.js version is correctly set to $(NODE_VERSION)."; \
	fi
.PHONY: local-check-node-version

local-git-sync:
	@echo "Checking if there are any changes in local..."
	@git diff-index --quiet HEAD || (echo "$(ERROR_COLOR)Local git changes detected. Please commit or stash your changes before syncing..$(NORMAL_COLOR)"; exit 1)
	@git checkout $(GIT_MASTER_BRANCH)
	@echo "Pulling latest changes..."
	@git pull origin $(GIT_MASTER_BRANCH)
	@echo "Local is up to date."
.PHONY: local-git-sync

local-sync-uploads:
	@echo "Checking if $(BACKUP_DIR)/uploads_$(DATE).tar.gz file exists..."
	@if [ ! -f "$(BACKUP_DIR)/uploads_$(DATE).tar.gz" ]; then \
		echo "$(ERROR_COLOR)Uploads backup file $(BACKUP_DIR)/uploads_$(DATE).tar.gz does not exist.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Removing existing uploads directory..."
	@if [ -d "${PROJECT_WEB_ROOT}/uploads" ]; then \
		rm -rf ${PROJECT_WEB_ROOT}/uploads; \
		echo "Removed ${PROJECT_WEB_ROOT}/uploads"; \
	fi
	@echo "Extracting uploads..."
	@tar -xzf $(BACKUP_DIR)/uploads_$(DATE).tar.gz -C ${PROJECT_WEB_ROOT}
	@echo "Uploads synced."
.PHONY: local-sync-uploads

local-start-ddev:
	@echo "Checking if ddev is installed..."
	@if ! command -v ddev &> /dev/null; then \
		echo "$(ERROR_COLOR)ddev is not installed. Please install ddev first.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Starting ddev..."
	@ddev start
	@echo "Ddev started."
.PHONY: local-start-ddev

local-composer-install:
	@echo "Installing composer dependencies..."
	@ddev composer install
	@echo "Composer dependencies installed."
.PHONY: local-composer-install

local-db-restore:
	@echo "Tip, if you see error message ./shell/select_backup_db.sh: Permission denied, run $(WARNING_COLOR)chmod +x ./shell/select_backup_db.sh$(NORMAL_COLOR)"
	@echo "Checking if backup directory exists..."
	@if [ ! -d "$(BACKUP_DIR)" ]; then \
		echo "Backup directory $(BACKUP_DIR) does not exist."; \
		exit 1; \
	fi
	@echo "Restoring database..."
	@./shell/select_backup_db.sh $(BACKUP_DIR)
	@if [ ! -f /tmp/selected_backup_file.txt ]; then \
		echo "$(WARNING_COLOR)No backup file selected.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@SELECTED_FILE=$$(cat /tmp/selected_backup_file.txt); \
		echo "Using backup file: $$SELECTED_FILE"; \
		ddev craft db/restore $$SELECTED_FILE; \
		rm /tmp/selected_backup_file.txt
	@echo "Database restored."
.PHONY: local-db-restore

local-clear-logs:
	@echo "Clearing your local logs..."
	@rm -rf $(LOG_DIR)/*
	@echo "Logs cleared."
.PHONY: local-clear-logs

local-switch-maintenance-branch:
	@echo "Checking out to maintenance branch..."
	@git checkout -b maintenance/$(DATE)
	@echo "Switched to maintenance branch."
.PHONY: local-switch-maintenance-branch

local-clear-craft-caches:
	@echo "Clearing craft caches..."
	@ddev craft clear-caches/all
	@echo "Craft caches cleared."
.PHONY: local-clear-craft-caches

local-clear-yump-caches:
	@echo "Clearing yump caches..."
	@rm -rf $(YUMP_CACHE_DIR)/*.json
	@echo "Yump caches cleared."
.PHONY: local-clear-yump-caches

local-maintenance:
	@echo "Starting local maintenance process..."
	@make local-check-node-version
	@make local-git-sync
	@make local-sync-uploads
	@make local-start-ddev
	@make local-composer-install
	@make local-db-restore
	@make local-clear-logs
	@make local-switch-maintenance-branch
	@make local-clear-craft-caches
	@make local-clear-yump-caches
	@echo "Local maintenance process completed."
.PHONY: local-maintenance

############################################################################################################
# 5. Prepare deploy to staging on local
local-check-error-log: 
	@echo "Checking if there are any error logs..."
	@if [ -f "$(LOG_DIR)/phperrors.log" ]; then \
		echo "$(ERROR_COLOR)phperrors.log found in storage, please review and fix them before proceeding.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Keywords to check: $(ERROR_LOG_KEYWORDS)"
	@for keyword in $(ERROR_LOG_KEYWORDS); do \
		echo "Checking for keyword: $$keyword"; \
		if grep -H "$$keyword" $(LOG_DIR)/*.log > /dev/null 2>&1; then \
			echo "$(ERROR_COLOR)\"$$keyword\" keyword found in $(LOG_DIR). Please check and fix them before proceeding.$(NORMAL_COLOR)"; \
			echo "You can copy and run $(WARNING_COLOR)'grep -H \"$$keyword\" $(LOG_DIR)/*.log'$(NORMAL_COLOR) to see the error logs."; \
			exit 1; \
		fi \
	done; \
	echo "No error logs found. Proceeding..."
.PHONY: local-check-error-log

local-npm-run-build:
	@echo "Checking if nvm is installed..."
	@if [ -z "$$NVM_DIR" ]; then \
		echo "$(ERROR_COLOR)nvm is not installed. Please install nvm first.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Checking if node_modules exists..."
	@if [ ! -d "node_modules" ]; then \
		echo "$(ERROR_COLOR)node_modules does not exist. Please run npm install first.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Switching to Node.js version $(NODE_VERSION)..."
	@source $$NVM_DIR/nvm.sh && nvm use $(NODE_VERSION) && \
		echo "Running npm run build..." && \
		npm run build && \
		echo "Build completed."
.PHONY: local-npm-run-build

local-git-add-commit:
	@echo "Adding all changes to git..."
	@git add .
	@git commit -m "maintenance"
	@echo "Changes added and committed with message 'maintenance'."
.PHONY: local-git-add-commit

local-git-push-maintenance:
	@echo "Pushing changes to maintenance branch..."
	@git push origin maintenance/$(DATE)
	@echo "Changes pushed to maintenance branch."
.PHONY: local-git-push-maintenance

local-prepare-staging-deploy:
	@echo "Starting staging deployment process..."
	@make local-check-error-log
	@make local-npm-run-build
	@make local-git-add-commit
	@make local-git-push-maintenance
	@echo "Staging deployment process completed."
.PHONY: local-prepare-staging-deploy

############################################################################################################
# 6. Test on staging server, and finish the maintenance report

check-log-file:
	@echo "Checking if there are any error logs..."
	@if [ -f "$(LOG_DIR)/phperrors.log" ]; then \
		echo "phperrors.log found in storage, please review and fix them before proceeding."; \
		exit 1; \
	fi
	@echo "Keywords to check: $(ERROR_LOG_KEYWORDS)"
	@for keyword in $(ERROR_LOG_KEYWORDS); do \
		echo "Checking for keyword: $$keyword"; \
		if grep -H "$$keyword" $(LOG_DIR)/*.log > /dev/null 2>&1; then \
			echo "\"$$keyword\" keyword found in $(LOG_DIR). Please check and fix them before proceeding."; \
			echo "You can copy and run 'grep -H \"$$keyword\" $(LOG_DIR)/*.log' to see the error logs."; \
			exit 1; \
		fi \
	done; \
	echo "No error logs found. Proceeding..."
.PHONY: check-log-file

############################################################################################################
# 7. Deploy on production
# Run composer install and create a backup of vendor at the sametime
prod-composer-install:
	@echo "Removing vendor.tar.gz if exist..."
	@rm -rf vendor.tar.gz
	@echo "Creating a backup of vendor..."
	@tar -czf vendor.tar.gz vendor
	@echo "Backup created: vendor.tar.gz"
	@echo "Removing vendor..."
	@rm -rf vendor
	@echo "Installing composer freshly..."
	@composer install --no-dev
	@echo "Composer installed."
.PHONY: prod-composer-install

############################################################################################################
# 8. Clean up local
# Clean up merged branches
local-clean-merged-branches:
	@echo "Checking for uncommitted changes..."
	@if ! git diff-index --quiet HEAD --; then \
		echo "$(ERROR_COLOR)Uncommitted changes found. Please commit or stash your changes before proceeding.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Switching to $(GIT_MASTER_BRANCH) branch..."
	@if ! git checkout $(GIT_MASTER_BRANCH); then \
		echo "$(ERROR_COLOR)Failed to checkout to $(GIT_MASTER_BRANCH) branch.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Pulling latest changes..."
	@if ! git pull origin $(GIT_MASTER_BRANCH); then \
		echo "$(ERROR_COLOR)Failed to pull latest changes.$(NORMAL_COLOR)"; \
		exit 1; \
	fi
	@echo "Cleaning up merged branches to $(GIT_MASTER_BRANCH)..."
	@for branch in $$(git branch --merged $(GIT_MASTER_BRANCH) | grep -v '^\*' | grep -v '$(GIT_MASTER_BRANCH)' | grep -v '$(GIT_STAGING_BRANCH)' | sed 's/ //g'); do \
		if git show-ref --verify --quiet refs/heads/$$branch; then \
			if git branch -d "$$branch"; then \
				echo "Deleted branch $$branch"; \
			else \
				echo "Forcing deletion of branch $$branch"; \
				git branch -D "$$branch"; \
			fi \
		fi; \
	done
	@echo "Merged branches cleaned."
.PHONY: local-clean-merged-branches

# Clean up zip files created in above tasks
local-clean-zip-files:
	@echo "Cleaning up zip files..."
	@rm -rf $(BACKUP_DIR)/*.tar.gz
	@echo "Zip files cleaned."
.PHONY: local-clean-zip-files