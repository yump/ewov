/* 
https://codepen.io/joelstrohmeier/pen/bGEMpmz
Credit to Alastair Campbell for providing the basis of this script 
*/

// Link up the cards in a way where:
// we don't have adjacent links to the same destination 
// user can still highlight text
function linkBoxes() {

    var singleLinkBoxes = document.querySelectorAll(".card--single-link");
    singleLinkBoxes.forEach(function (box) {
        var link = box.querySelector("a");
        if (link) {
            var url = link.getAttribute("href");
            box.addEventListener("click", function () {
                location.href = url;
                link.preventDefault;
            });
            box.classList.add("linkify");
            link.addEventListener("focus", function () {
                box.classList.add("isfocused");
            });
            link.addEventListener("blur", function () {
                box.classList.remove("isfocused");
            });

            /* Inject CTA in to cards as visual affordance but hide from assistive tech announcements */
            // box.insertAdjacentHTML("beforeend", '<span class="cta" aria-hidden="true">Read more</span>'); 
        }
    });

    var multiLinkBoxes = document.querySelectorAll(".card--multi-link");
    multiLinkBoxes.forEach(function (box) {
        var link = box.querySelector(".card__footer-btn--primary");

        if (link) {
            var url = link.getAttribute("href");
            console.log(box);
            var clickAreas = box.querySelectorAll(".card-clickable-area");
            console.log(clickAreas);
            clickAreas.forEach(function (clickArea) {
                clickArea.addEventListener("click", function () {
                    location.href = url;
                    link.preventDefault;
                });
                clickArea.classList.add("linkify");
                link.addEventListener("focus", function () {
                    clickArea.classList.add("isfocused");
                });
                link.addEventListener("blur", function () {
                    clickArea.classList.remove("isfocused");
                });
            });
        }

    });    

}

document.addEventListener("DOMContentLoaded", function () {
    if ("querySelector" in document) {
        linkBoxes();
    }
});
