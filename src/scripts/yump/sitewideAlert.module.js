  
$('[data-alert]').each(function () {
    const id = $(this).attr('data-alert');
    maybeOpenAlertBox(id);
});

$('[data-alert-close]').on('click', function (e) {
    // Do not perform default action when button is clicked
    e.preventDefault();
    const target = $(this).attr('data-alert-close');
    closeAlert(target);
});

/**
 * Closes the alert
 */
function closeAlert(alertID) {
    setCookie('alert-' + alertID, 'closed', 2);
    jQuery('[data-alert="' + alertID + '"]').hide();
}

function maybeOpenAlertBox(alertID) {
    // Check if alert has been closed
    if (getCookie('alert-' + alertID) !== 'closed') {
        jQuery('[data-alert="' + alertID + '"]').show().css({
            display: "flex"
        });
    }
}

/**
 * Sets Cookie
 * @param {str} key
 * @param {str} value 
 * @param {int} expiry in days
 */

function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
}