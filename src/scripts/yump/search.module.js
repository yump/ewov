/**
 * Filtering
 **/

function updateQuery(checkboxs) {
    //console.log(checkboxes);
    var uri = location.href;
    var updatedURI = uri;
    var value = 1;
    var key = '';
    // console.log(uri);
    $(checkboxes).each(function (index, checkbox) {
        key = checkbox.value;

        if (checkbox.checked) {
            value = 1;
            updatedURI = updateQueryStringParameter(updatedURI, key, value);
        } else {
            value = 0;
            updatedURI = updateQueryStringParameter(updatedURI, key, value);
        }
    });
    redirectPage(updatedURI);
}

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}

function redirectPage(uri) {
    document.location.replace(uri);
}

var checkboxes = $('.refine-search-controls .form-check-input');

$('#search-button').click(function () {
    updateQuery($(checkboxes));
});

// toggle search on the top 
function checkForInput(element) {
    const $label = $('.search__submit-btn');

    if ($(element).val().length > 0) {
        $label.addClass('search__submit-btn--input-has-text');
        return true;
    } else {
        $label.removeClass('search__submit-btn--input-has-text');
        return false;
    }
}

function toggleSearchBar() {
    $('.search-bar--desktop').toggleClass('search-bar--open');
    $('.search-bar--desktop').slideToggle();
    if ($('.search-bar--desktop').hasClass('search-bar--open')) {
        $('.search-bar__input').focus();
    }
}

$('.search-button__toggle-icon, .search-bar__close-btn').on('click', toggleSearchBar);

// The lines below are executed on page load
$('.search__input').each(function () {
    checkForInput(this);
});

// The lines below (inside) are executed on change & keyup
$('.search__input').on('change keyup', function () {
    checkForInput(this);
});