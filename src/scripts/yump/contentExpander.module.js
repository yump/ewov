/**
 * Expand the truncated content
 * Requires:
 *      data-expand-wrap - the wrapping div
 *      data-expand - the trigger that expands the content
 *      data-expand-content - the target content to expand
 */

/**
 * expands the hidden content
 * @param {str} id 
 */
function expandContent(id) {
    $('[data-expand-content="' + id + '"]').slideDown();
    $('[data-expand-wrap="' + id + '"]').addClass('expand-content--showing');
}

$('[data-expand]').each(function () {
    const id = $(this).attr('data-expand');
    $(this).on('click', function () {
        expandContent(id);
        $(this).attr('aria-expanded', 'true');
    });
});