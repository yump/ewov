$('.nav-tabs__link--mobile').on('click', function (e) {
    e.preventDefault();
    $(this).tab('show');
    var el = $(this);
    $('.nav-tabs__link--mobile').removeClass('active');
    el.addClass('active');
});