const slick = require('slick-carousel')
const debounce = require('debounce');

jQuery(document).ready(function($){
    $('.image-slider-wrap').each(function() {
		var self = this;
		$(self).on('init', function(event, slick){
			resetImageSlick(self, slick);
            var activeIndex = slick.slickCurrentSlide();
            updateCaption($(self), activeIndex, slick);
		});
	});

    if($('.image-slider-wrap').length){
    // $('.image-slider-wrap').on('init', function(event, slick){
    //     var $currentSlide = $(this).find('.slick-slide.slick-current');
    //     updateHasCaptionClass($(this), $currentSlide);
    // });
    // $('.image-slider-wrap').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    //     var $nextSlide = $(this).find('.slick-slide[data-slick-index="' + nextSlide + '"]');
    //     updateHasCaptionClass($(this), $nextSlide);
    // });
    $('.image-slider-wrap').slick({
        dots: true,
        // autoplay: true,
        pauseOnHover: true,
        pauseOnDotsHover: true,
        autoplaySpeed: 5000,
        adaptiveHeight: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button class="slider__nav-button slider__nav-button--left"><figure class="slider__nav-button-ic-arrow slider__nav-button-ic-arrow--left"><img alt="" src="/assets/img/svg/ic-chevron-left.svg" /></figure></button>',
        nextArrow: '<button class="slider__nav-button slider__nav-button--right"><figure class="slider__nav-button-ic-arrow slider__nav-button-ic-arrow--right"><img alt="" src="/assets/img/svg/ic-chevron-right.svg" /></figure></button>',
    });

        

    $('.image-slider-wrap').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        // console.log(nextSlide);
        updateCaption($(this), nextSlide, slick);
    });
    
        
    }
    $(window).on('resize', debounce(function() {
    	$('.image-slider-wrap').each(function() {
    		if($(this).hasClass('slick-initialized')) {
    			resetImageSlick(this, $(this).slick('getSlick'));
    		}
    	});
	}, 250));

    // // turn off autoplay to avoid page jumping after scrolling passes a certain point.
    // $(window).on('scroll', function() {
    //     $('.image-slider-wrap').each(function() {
    //         if($(this).hasClass('slick-initialized')) {
    //             var slick = $(this).slick('getSlick');
    //             if($(window).scrollTop() > $(this).offset().top + $(this).find('.slick-list').height() / 2) {
    //                 if(slick.slickGetOption('autoplay')) {
    //                     slick.slickSetOption('autoplay', false, true);
    //                 }
    //             } else {
    //                 if(!slick.slickGetOption('autoplay')) {
    //                     slick.slickSetOption('autoplay', true, true);
    //                 }
    //             }
    //         }
    //     });
    // }).trigger('scroll');
});

function resetImageSlick(target, slick) {
	slick.setPosition();
	$(target).css({'visibility': 'visible', 'position': 'relative'});
}

function updateCaption($slider, activeIndex, slick) {
    var $parent = $slider.closest('.slider');
    var $activeSlide = $slider.find('.slick-slide[data-slick-index="' + activeIndex + '"]');
    var $caption = $activeSlide.find('.slider-item__caption');
    var $counterText = activeIndex + 1 + '/' + slick.slideCount;
    if ($caption.length) {
        $parent.find('.slider-item__caption-text').html($caption.html());
        $parent.find('.slider-item__caption-text').slideDown();
    } else {
        $parent.find('.slider-item__caption-text').slideUp().empty();
    }
    $parent.find('.slider-item__caption-counter').html($counterText);
    $parent.find('.slider__captions').slideDown();

}

/**
 * This class 'current-slide-has-caption' is for us to determine the position of the dots, otherwise it might be overlapped with the caption text
 * @param  {[type]} $slider [description]
 * @param  {[type]} $slide  [description]
 * @return {[type]}         [description]
 */
// function updateHasCaptionClass($slider, $slide) {
//     if($slide.find('.slider-item__has-caption').length) {
//         $slider.addClass('current-slide-has-caption');
//     } else {
//         $slider.removeClass('current-slide-has-caption');
//     }
// }
