if(!window.Yump) {
	window.Yump = {};
}

// detect IE and add 'ie' class to body
Yump.isIE = "ActiveXObject" in window; //window.ActiveXObject !== undefined;
if(Yump.isIE) {
	document.body.classList.add("ie");
}
