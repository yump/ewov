/**
 * Stories Slider
 **/

// Desktop Button Nav
let buttons = document.querySelectorAll('.stories__nav-dt-item-button');
let targets = document.querySelectorAll('.story');

for (let index = 0; index < buttons.length; index++) {

    const button = buttons[index];

    button.onclick = function (e) {

        const target = document.getElementById(button.dataset.trigger);

        // Update Story
        for (let index = 0; index < targets.length; index++) {
            const story = targets[index];
            story.classList.remove('story--active');
        }
        target.classList.toggle('story--active');

        // Update Button
        for (let index = 0; index < buttons.length; index++) {
            const button = buttons[index];
            button.classList.remove('stories__nav-dt-item-button--active');
        }        
        button.classList.add('stories__nav-dt-item-button--active');
        
    };
}

// Mobile Swipe and Button Nav
// adapted from https://codepen.io/foleyatwork/pen/ylwoz
var slider = {

    el: {
        slider: $(".stories"),
        holder: $(".stories__wrap"),
        navButton: $(".stories__nav-mobile-button")
        // imgSlide: $(".slide-image")
    },

    slideWidth: $('.stories').width(),
    storiesLength: $('.story').length - 1,
    touchstartx: undefined,
    touchmovex: undefined,
    movex: undefined,
    index: 0,
    longTouch: undefined,

    init: function () {
        this.bindUIEvents();
    },

    bindUIEvents: function () {

        this.el.holder.on("touchstart", function (event) {
            slider.start(event);
        });

        this.el.holder.on("touchmove", function (event) {
            slider.move(event);
        });

        this.el.holder.on("touchend", function (event) {
            slider.end(event);
        });

        this.el.navButton.on("click", function (event) {
            const direction = $(this)[0].dataset.direction;
            slider.slide(event, direction);
        });

    },

    start: function (event) {
        // Test for flick.
        this.longTouch = false;
        setTimeout(function () {
            window.slider.longTouch = true;
        }, 250);

        // Get the original touch position.
        this.touchstartx = event.originalEvent.touches[0].pageX;

        // The movement gets all janky if there's a transition on the elements.
        $('.stories__wrap--animate').removeClass('animate');
    },

    move: function (event) {
        // Continuously return touch position.
        this.touchmovex = event.originalEvent.touches[0].pageX;
        // Calculate distance to translate holder.
        this.movex = this.index * this.slideWidth + (this.touchstartx - this.touchmovex);
        // Defines the speed the images should move at.
        var panx = 100 - this.movex / 6;
        console.log(this.movex);
        if (this.movex < 600) { // Makes the holder stop moving when there is no more content.
            this.el.holder.css('transform', 'translate3d(-' + this.movex + 'px,0,0)');
        }
        if (panx < 100) { // Corrects an edge-case problem where the background image moves without the container moving.
            // this.el.imgSlide.css('transform', 'translate3d(-' + panx + 'px,0,0)');
        }
    },

    end: function (event) {
        // Calculate the distance swiped.
        var absMove = Math.abs(this.index * this.slideWidth - this.movex);
        // Calculate the index. All other calculations are based on the index.
        if (absMove > this.slideWidth / 2 || this.longTouch === false) {
            if (this.movex > this.index * this.slideWidth && this.index < 2) {
                this.index++;
            } else if (this.movex < this.index * this.slideWidth && this.index > 0) {
                this.index--;
            }
        }
        // Move and animate the elements.
        this.el.holder.addClass('animate').css('transform', 'translate3d(-' + this.index * this.slideWidth + 'px,0,0)');
        // this.el.imgSlide.addClass('animate').css('transform', 'translate3d(-' + 100 - this.index * 50 + 'px,0,0)');

    },

    backToStart: function(event) {
        if (this.index != 0) {
            this.index--;
            slider.slide(null, 'left');
        }
    },

    slide: function(event, direction) {
        if (direction == 'right') {
            if (this.index != this.storiesLength) {
                this.index++;
            } else {
                this.backToStart(event);
            }
        } else {
            if (this.index != 0) {
                this.index--;
            } else {
                // console.log('at start');
            }
        }
        this.el.holder.addClass('stories__wrap--animate').css('transform', 'translate3d(-' + this.index * this.slideWidth + 'px,0,0)');
    }

};

window.slider = slider;

slider.init();

$(window).on('resize', function () {
    let wrapperWidth = $('.stories').width();
    slider.slideWidth = wrapperWidth;
    // slide back to start to fix css issues on resizing the window
    slider.slide(null, 'left');
});