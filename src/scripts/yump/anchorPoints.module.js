$(window).on('unload', function () {
    $(window).scrollTop(0);
});

$(document).ready(function (e) {

    $(".anchor-links__link").click(function (e) {
        e.preventDefault();
        var urlLink = $(this).data("link");
        var pos = $(urlLink).offset().top - 10;
        $('html, body').animate({
            scrollTop: pos
        }, 800);
        window.location.hash = urlLink;
        $(urlLink).focus();
    });

    if (window.location.hash) {
        var hash = window.location.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 500);
    };

});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})