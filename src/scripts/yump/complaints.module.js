function showMore(target, speed) {
    $('.' + target).find('.issues__issue--mobile-hide').slideToggle(speed, function(){
        if ($(this).is(':visible'))
            $(this).css('display', 'flex');
    });
    $('.' + target).find('.issues__type-heading--mobile-hide').slideToggle(); 
}

function updateButton(button) {
    if (button.find('.btn__text').text() == 'Show more') {
        button.find('.btn__text').text('Show less');
    } else {
        button.find('.btn__text').text('Show more');
    }   
}

$('.issues__more--mobile').on('click', function(){
    let group = $(this).data('target-group');
    showMore(group, 'fast');
    $(this).hide();
    // updateButton($(this));
});

$('.issues__more--dt').on('click', function () {
    let group = $(this).data('target-group');
    showMore(group, 'medium');
    $(this).hide();
    // updateButton($(this));
});