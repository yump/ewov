<?php
/**
 * Craft web bootstrap file
 */
$sessionPath = getcwd().'/storage/sessions';
if(!is_dir($sessionPath)){
    mkdir($sessionPath);
}

ini_set('session.save_path', $sessionPath);
ini_set('session.cache_expire', 240);

// Set path constants
define('CRAFT_BASE_PATH', dirname(__FILE__));
define('CRAFT_VENDOR_PATH', CRAFT_BASE_PATH.'/vendor');

// Load Composer's autoloader
require_once CRAFT_VENDOR_PATH.'/autoload.php';

// Load dotenv?
if (class_exists('Dotenv\Dotenv') && file_exists(CRAFT_BASE_PATH.'/.env')) {
    Dotenv\Dotenv::createUnsafeImmutable(CRAFT_BASE_PATH)->safeLoad();
}

// Load and run Craft
define('CRAFT_ENVIRONMENT', getenv('ENVIRONMENT') ?: 'production');
$app = require CRAFT_VENDOR_PATH.'/craftcms/cms/bootstrap/web.php';
$app->run();
