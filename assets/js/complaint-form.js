Vue.component('selectize', {
    props: ['options', 'value'],
    template: '<select><slot></slot></select>',
    mounted: function () {
        var vm = this;
        var opt = $.extend({plugins: ['remove_button']}, $(this.$el).data());
        if (this.options != null)
            opt.options = this.options;
        this.sel = $(this.$el).selectize(opt)
            .on("change", function () {
                vm.$emit('input', vm.sel.getValue());
            })[0].selectize;
        this.sel.setValue(this.value, true);
    },
});
new Vue({
    delimiters: ['${', '}'],
    el: ".vue"
});