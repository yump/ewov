//document: https://all.docs.genesys.com/WID/Current/SDK/WebChat-combined
//
window._genesys = {
    "widgets":{
        "webchat": {
            "transport": {
                "type": "purecloud-v2-sockets",
                "dataURL": "https://api.mypurecloud.com.au",
                "deploymentKey": "3250c838-4eaa-4d5e-8e07-d779fdb73c35",  // Add in the Deployment Key you got when creating the widget integration e.g. "abd51fbe-3856-438c-b50d-337326b1fe89"
                "orgGuid": "f2153040-8fb0-43d0-b4e7-b647f3abc0eb",        // Add in the Org ID which can be gotten in Admin \ Organization Settings \ Organisation Details \ Advanced
                "interactionData": {
                    "routing": {
                        "targetType": "QUEUE",  // Note that this is ignored if in your webchat widget config in GenesysCloud you are routing to a flow.
                        "targetAddress": "",    // enter the queue name, as above this is ignored if routing to a flow but is good for reference.
                        "priority": 2           // enter the queue name, as above this is ignored if routing to a flow but is good for reference.
                    }
                }
            },
            "userData": {
                "customField1Label": "Existing Case",
                "customField2Label": "Type",
                "customField3Label": "Supply"
            },
            "chatButton": {
                "enabled": true,
                "template": '<div class="cx-widget cx-webchat-chat-button cx-side-button" role="button" tabindex="0" data-message="ChatButton" data-gcb-service-node="true" onclick="customPlugin.command(\'WebChat.open\', getAdvancedConfig())"><span class="cx-icon" data-icon="chat"></span><span class="i18n cx-chat-button-label" data-message="ChatButton"></span></div>',
                "effect": 'fade',
                "openDelay": 1000,
                "effectDuration": 300,
                "hideDuringInvite": true
            },
        },
        "main": {
            "theme": "light",
            "i18n": {
                "en": {
                    "webchat": {
                        "ChatTitle": "EWOV Live Chat",
                        "AriaWindowLabel": "EWOV Chat Window",
                        "AriaMinimize": "EWOV Chat Minimize",
                        "AriaMaximize": "EWOV Chat Maximize",
                        "AriaClose": "EWOV Chat Close"
                    }
                }
            }
        }
    },
};

// Note that only the Firstname and Email address fields below are validated

function getAdvancedConfig() {
    return {
        "form": {
            "autoSubmit": false,
            "firstname": "",
            "lastname": "",
            "email": ""
        },
        "formJSON": {
            "wrapper": "<table></table>",
            "inputs": [
                {
                    "id": "cx_webchat_form_firstname",
                    "name": "firstname",
                    "maxlength": "100",
                    "placeholder": "Required",
                    "label": "First Name",
                    "validateWhileTyping": false, // default is false - this requires at least 1 character entered before you can start chat
                    "validate": function (event, form, input, label, $, CXBus, Common) {
                        if (input && input.val() && (input.val()).length >= 1) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                {
                    "id": "cx_webchat_form_lastname",
                    "name": "lastname",
                    "maxlength": "100",
                    "placeholder": "Required",
                    "label": "Last Name",
                    "validateWhileTyping": false, // default is false - this requires at least 1 character entered before you can start chat
                    "validate": function (event, form, input, label, $, CXBus, Common) {
                        if (input && input.val() && (input.val()).length >= 1) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                {
                    "id": "cx_webchat_form_email",
                    "name": "email",
                    "maxlength": "100",
                    "placeholder": "Optional",
                    "label": "Email"
                },
                {
                    "id": "cx_webchat_form_customselect1",
                    "name": "customField1",
                    "maxlength": "100",
                    "placeholder": "Select an option",
                    "label": "Have you contacted EWOV <br/> about this issue before?",
                    "type": "select",
                    "options": [
                        {
                            "text": "Optional",
                            "value": "Not Selected"
                        },
                        {
                            "text": "Yes",
                            "value": "Yes"
                        },
                        {
                            "text": "No",
                            "value": "No"
                        }
                    ],
                    "wrapper": "<tr><th>{label}</th><td>{input}</td></tr>"
                },
                {
                    "id": "cx_webchat_form_customselect2",
                    "name": "customField2",
                    "maxlength": "100",
                    "placeholder": "Select an option",
                    "label": "Which supply is your matter about?",
                    "type": "select",
                    "options": [
                        {
                            "text": "Optional",
                            "value": "Not Selected"
                        },
                        {
                            "text": "Gas",
                            "value": "Gas"
                        },
                        {
                            "text": "Electricity",
                            "value": "Electricity"
                        },
                        {
                            "text": "Water",
                            "value": "Water"
                        },
                        {
                            "text": "Other",
                            "value": "Other"
                        }
                    ],
                    "wrapper": "<tr><th>{label}</th><td>{input}</td></tr>"
                },
                {
                    "id": "cx_webchat_form_customselect3",
                    "name": "state",
                    "maxlength": "100",
                    "placeholder": "Select an option",
                    "label": "Is the property in VIC?",
                    "type": "select",
                    "options": [
                        {
                            "text": "Optional",
                            "value": "Not Selected"
                        },
                        {
                            "text": "Yes",
                            "value": "In VIC"
                        },
                        {
                            "text": "No",
                            "value": "Not in VIC"
                        }

                    ],
                    "wrapper": "<tr><th>{label}</th><td>{input}</td></tr>"
                },
                {
                    "id": "cx_webchat_form_customselect4",
                    "name": "customField3",
                    "maxlength": "100",
                    "placeholder": "Select an option",
                    "label": "Has the supply been disconnected <br/> or restricted without <br/> your permission?",
                    "type": "select",
                    "options": [
                        {
                            "text": "Optional",
                            "value": "Not Selected"
                        },
                        {
                            "text": "Yes",
                            "value": "Disconnected"
                        },
                        {
                            "text": "No",
                            "value": "Connected"
                        }

                    ],
                    "wrapper": "<tr><th>{label}</th><td>{input}</td></tr>"
                }
            ]
        }
    };
}

const customPlugin = CXBus.registerPlugin('Custom');

+$(function () {
    customPlugin.command('WebChat.showChatButton', getAdvancedConfig());
});
