# Maintenance Log

## To read before conducting Maintenance
Latest log entry should be inserted at the top

Docs: https://ewov.com.au/docs/

Particular part worth looking at during maintenance:

- Check 'not-handled' redirected URLs and see if there are any popular hits worth redirecting. Ignore the bot-like hits, e.g. /wp-login. Be sensitive of whether it should be in scope to fix them.

#### Important items to check on production:

### 15 Nov 2022 - by Padam
* Updated Craft and all plugins to the latest version on both staging and production:
  - craft 3.7.51 => 3.7.59
  - retour 3.1.73 => 3.2.3
  - seomatic 3.4.37 => 3.4.42
  - blitz 3.12.5 => 3.12.6
  - freeform 3.13.17 => 3.13.21
  - imager-x v3.6.2 => v3.6.5
  - tablemaker 3.0.1 => 3.0.4
  - Sprout Fields 3.8.5 =>3.8.6

* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages 
  - News and News details - all good
  - Fact Sheets, Fact Sheet detail page, print - all good
  - Reports - all good
  - Search function - all good
  - Share feature - all good
  - Form Submissions ( Validation, catch submission in admin, email sent feature) - all good
 
* cPanel:  File Usage - 165,354 / 500,000 (33.07%)  - all good
* cPanel: Disk Usage: 7.86 GB / 50 GB (15.72%) - all good
* cPanel: Memory Usage: 322.7 MB / 8 GB (3.94%) - all good
* cPanel: Mysql Disk Usage: 713.04 MB / 42.84 GB (1.63%) - all good
* cPanel: error logs - all good
* Google search console - all good
* Google Analytics data collection checked - all good
* Daily backup (Acorns Backup) - all good
* Broken links:none - all good

### 03 May 2022 - by Wei Lin
* Craft CMS plugin updates:
  - craft 3.7.35 => 3.7.39
  - blitz 3.11.1 => 3.12.2
  - freeform 3.13.1 => 3.13.8
  - imager-x v3.5.5 => v3.5.8
  - neo 2.12.5 => 2.13.6
  - redactor 2.10.5 => 2.10.7
  - retour 3.1.70 => 3.1.71
  - seomatic 3.4.26 => 3.4.30
* Check auto-backup is running correctly - Yes
* Checked Craft log for errors - nothing to be concerned
* Testing
  - Test CMS Functionality
  - Test content blocks
  - Test cache (refresh cache and make sure it is generated correctly)
  - Test Front end templates
  - Test Fact Sheets functionality
  - Test Reports functionality
  - Test SEO functionalities
  - Test Form Submissions
* Checked server usage - all still under 50%, so all good 
* Double check page view traffics are collected in Google Analytics correctly - yes.
* Check Google Search Console for any coverage issues
* Security improvements
  - Prevent cache poisoning vulnerability
  - Enforce samesitecookievalue
  - preventUserEnumeration
* Scan for broken links

Next steps for you / Issues found:
- The 403 issue on Google Search Console still hasn't been validated by Google yet. Found the root cause, fixed the issue, and requested Google to revalidate the issue again.
- Look into the Freeform spam issue (that all form submissions go to spam). 
  - We found setting "Form Submit Expiration" field can cause this issue. Setting this field is unnecessary now as the complaint form has been hosted on Salesforce externally. We removed this setting to fix the issue.
- We noticed that we did not give you access to manage all the forms in Freeform. They are now given.
- Broken links:
https://www.oaic.gov.au/privacy/privacy-registers/privacy-codes-register/cr-code
error code: 404 (not found), linked from page(s):
  https://www.ewov.com.au/fact-sheets/credit-default-listings-and-debt-collection
- We decided not to provide "Not handled URLs" report to all our clients from now on. The reason is that we found it not very useful lately. We looked deeper into the reported URLs and found the hits are mostly from bots (e.g. Google, Bing, etc). We now rely on Google Search Console to report any links submitted in the sitemap becomes 404, and it's in our maintenance checklist to review Google Search Console.
- Found the SEO Description for "Fact Sheets" section is not correctly set (was set to custom, so all fact sheets use site's global description). Not it's been set to from "Summary" field.

### 28 Feb 2022 - by Parry
* Craft CMS plugin updates:
  - craft 3.7.23 => 3.7.35
  - blitz 3.11.0 => 3.11.1
  - freeform 3.12.6 => 3.13.1
  - imager-x v3.5.4 => v3.5.5
  - neo 2.11.9 => 2.12.5
  - redactor 2.8.8 => 2.10.5
  - retour 3.1.67 => 3.1.70
  - seomatic 3.4.20 => 3.4.26
* CMS functionality - all good
* Blocks - all good
* Error logs - all good
* Switching production server to use php 7.4
* Sync staging site with the latest production site
* Google search console
- found 55 pages crawled as 403 response, already submitted fix validation, wait for Google's next crawl
- found 1 page crawled as 500 response (https://ewov.com.au/complaint) already correct 500 response
  It's already removed from latest sitemap, already submitted fix validation, wait for Google's next crawl
  Also found one redirect rule redirect to /complaint page at: https://www.ewov.com.au/admin/retour/edit-redirect/4
  updated it redirect to /start-a-complaint page
* Tested production site -- all good
* File Usage  210,523 / 300,000 (70.17%) - dropped 3% since last maintenance we still need to closely monitor it
* Disk Usage  8.79 GB / 25 GB (35.17%) - all good
* MySQL® Disk Usage  729.83 MB / 16.92 GB (4.21%) - all good
* Added help docs for Fact sheets
* Not handled redirects
*************************
- Link 
  Remote Ip, Hits, Last hit
*************************
- /files/styles/slideshow/public/images/slides/front-page-slider-cropped.jpg  
  173.252.107.28   1725	 2022-03-01 10:00:50
- /files/energy-button-345x200px.png                                          
  69.63.184.4      1221	 2022-03-01 20:09:46
- /pdfs/Responses/2006/MCE NERA Economic Consulting and Gilbert Tobin paper on Energy Distribution and Retail Regulation - EWOV comments.pdf
  66.249.79.65	   122	 2022-03-01 05:29:56
- /user/login
  95.108.213.31	   112   2022-03-01 09:48:28
- /reports/reflect-202202
  110.232.143.2	   24	 2022-02-23 04:51:53
- /pdfs/Responses/2007/070503-L-RPWG Supp Working Paper Enforcmt
  66.249.79.65	   8	 2022-02-28 19:11:12
- /pdfs/Resolution/Res 26/7463_Res26_Web_screen.pdf
  66.249.79.94	   8     2022-02-28 17:22:50
- /resources/hot-topics/how-is-the-price-of-electricity-and-gas-calculated-in-victoria-february-2014.
  154.54.249.203   7     2022-02-21 22:24:21
- /files/res-online-8.pdf
  66.249.79.92	   6     2022-02-23 07:51:21
* broken links
  http://www.anzoa.com.au/about-ombudsmen.html
  Linked from: https://www.ewov.com.au/about-us/who-we-are

### 29 Nov 2021 - by Parry
* Craft CMS plugin updates:
  "craftcms/cms": "3.7.10" => "3.7.23"
  "nystudio107/craft-retour": "3.1.61" => 3.1.67
  "nystudio107/craft-seomatic": "3.4.6" => 3.4.20
  "putyourlightson/craft-blitz": "3.10.2" => "3.11.0"
  "solspace/craft-freeform": "3.11.10" => "3.12.6"
* Upgrade imager to ImagerX "aelvan/imager": "v2.4.0" => "spacecatninja/imager-x": "v3.5.4"
* CMS functionality - all good
* Blocks - all good
* Error logs - all good
* Sync staging site with the latest production site
* Tested production site -- all good
* File Usage  217,845 / 300,000 (72.61%) - watch closely in case of getting over quota
* Disk Usage  8.43 GB / 25 GB (33.73%) - all good
* MySQL® Disk Usage  628.13 MB / 17.18 GB (3.57%) - all good
* Not handled redirects
  - /files/styles/slideshow/public/images/slides/front-page-slider-cropped.jpg hits 1590
  - /files/energy-button-345x200px.png hits 1130
  - /community/community-events/past/ hits 1079
  - /files/styles/participant_data/public/agl_sales_0.png hits 137
  - /start-a-complaint/fair-and-reasonable-investigation-framework hits 127
  - /community hits 115
  - /pdfs/Responses/2006/MCE NERA Economic Consulting and Gilbert Tobin paper on Energy Distribution and Retail Regulation - EWOV comments.pdf hits 69
  - /resources/case-studies/a-customer-complained-about-service-after-his-retailer-didn’t-provide-a hits 36
  - /reports/res-online/node/add hits 35
  - /node/add hits 34
  - /reports/connect hits 25
* broken links
- 503 Service unavailable
  http://www.clickenergy.com.au/
  Linked from: https://www.ewov.com.au/members/find-a-member
- Host not found
  https://www.vic.tasgas.com.au/
  Linked from: https://www.ewov.com.au/members/find-a-member
- 404 Not found
  http://ow.ly/ZPrzU
  Linked from: https://www.ewov.com.au/fact-sheets/credit-default-listings-and-debt-collection
  and https://www.ewov.com.au/files/fact-sheet-29-credit-default-listings-and-debt-collection_0.pdf
- 404 Not found
  https://www.powercor.com.au/power-outages/
  Linked from: https://www.ewov.com.au/fact-sheets/planned-and-unplanned-outages

### 31 Aug 2021 - by Parry
* Craft CMS plugin updates:
  "craftcms/cms": "3.6.15", => "3.7.10"
  "barrelstrength/sprout-fields": "3.8.4", => "3.8.5"
  "craftcms/redactor": "2.8.7", => "2.8.8"
  "doublesecretagency/craft-cpcss": "2.3.0", => "2.3.1"
  "nystudio107/craft-retour": "3.1.53", => "3.1.61"
  "nystudio107/craft-seomatic": "3.3.42", => "3.4.6"
  "putyourlightson/craft-blitz": "3.9.0", => "3.10.2"
  "solspace/craft-freeform": "3.11.4.1", => "3.11.10"
  "spicyweb/craft-neo": "2.9.8", => "2.11.9"
  "verbb/field-manager": "2.2.2", => "2.2.4"

* Important Notes: "aelvan/imager" is no longer supported, please consider update it to "spacecatninja/craft-imager-x". Cost USD$99 one time + USD$59 yearly on going 

* Checked file usage and other stats from cPanel
  - Disk Usage 5.98 GB / 25 GB (23.93%)
  - File Usage 123,519 / 300,000 (41.17%)
  - MySQL® Disk Usage 541.55 MB / 19.55 GB (2.71%)
* Checked the error log
  - Found [error][blitz] file_put_contents(/home/ewovcoma/public_html/cache/blitz/www.ewov.com.au/index.html): failed to open stream: Is a directory
    Fixed it by removing folder /home/ewovcoma/public_html/cache/blitz/www.ewov.com.au/index.html on live server, allowing file with same name to be created
    This should allow home page loading using blitz cache without problem
* Sync staging site with the latest production site
* Tested production site -- all good
* Not handled redirects (Columns: URL, hits, Last hit date & time). Please consider adding new redirect rules to the following links:
  - /files/styles/slideshow/public/images/slides/front-page-slider-cropped.jpg
    Hits: 1481  
    Last Hit: 31/8/21 2:04
  - /files/energy-button-345x200px.png  
    Hits: 972
    Last Hit: 31/8/21 4:20
  - /community/community-events/past/
    Last Referrer URL: https://www.ewov.com.au/community/community-events/past/
    Hits: 743
    Last Hit: 31/8/21 1:00
  - /reports/res-online/node/add
    Last Referrer URL: https://www.ewov.com.au/
    Hits: 75
    Last Hit: 30/8/21 8:24
  - /node/add
    Last Referrer URL: https://www.ewov.com.au/
    Hits: 74
    Last Hit: 30/8/21 8:24
  - /administrator/
    Last Referrer URL: https://www.ewov.com.au/administrator/
    Hits: 57
    Last Hit: 27/8/21 23:11
  - /about-us/tel:1800500509
    Last Referrer URL: https://www.ewov.com.au/about-us/tel:1800500509
    Hits: 53
    Last Hit: 29/8/21 1:00
  - /sites/all/themes/custom/calico/favicon.ico
    Hits: 49
    Last Hit: 30/8/21 16:35
  - /files/styles/participant_data/public/agl_sales_0.png
    Hits: 33
    Last Hit: 30/8/21 14:44
  - /about-us/careers/conciliator
    Hits: 24
    Last Hit: 30/8/21 22:01
  - /community/community-events/past
    Hits: 19
    Last Hit: 30/8/21 5:29
  - /pdfs/Responses/2006/MCE NERA Economic Consulting and Gilbert Tobin paper on Energy Distribution and Retail Regulation - EWOV comments.pdf
    Hits: 18
    Last Hit: 30/8/21 16:49
  - /files/example-energy-bill.pdf
    Hits: 17
    Last Hit: 26/8/21 16:30
  - /resources/case-studies/a-retailer-fails-to-action-the-cooling-off-period-november-2016
    Hits: 17
    Last Hit: 30/8/21 22:25
  - /resources/case-studies/delayed-solar-system-connection-results-in-700-in-lost-credits
    Hits: 16
    Last Hit: 30/8/21 20:20
  - /files/affordability-report-december-2016.pdf
    Hits: 16
    Last Hit: 30/8/21 20:24
  - /fact-sheets/high-energy-bills
    Hits: 15
    Last Hit: 31/8/21 4:01
  - /resources/case-studies/transfer-issue-leads-to-gas-disconnection
    Hits: 15
    Last Hit: 24/8/21 16:33
  - /resources/case-studies/miss-h-gets-a-call-from-a-debt-collector-after-a-retailer-fails-to-close-an
    Hits: 13
    Last Hit: 29/8/21 10:18
  - /files/fact_sheet_33_ewov-basics-indigenous-brochure-2020.pdf
    Hits: 13
    Last Hit: 26/8/21 22:27
  - /taxonomy/term/9/all/feed
    Hits: 13
    Last Hit: 30/8/21 4:09
* Docker Link Broken outbound links, please consider update the following link in the page:
  - 503 Service unavailable https://www.clickenergy.com.au/
    Linked from: https://www.ewov.com.au/members/find-a-member
  - 410 Gone http://supagas.com.au/
    Linked from: https://www.ewov.com.au/members/find-a-member
  - 404 Not found https://www.consumer.vic.gov.au/shopping/energy-products-and-services/solar-energy
    Linked from: https://www.ewov.com.au/about-us/faqs
  - 404 Not found http://agriculture.vic.gov.au/agriculture/farm-management/drought/dry-seasons-support/financial-support
    Linked from: https://www.ewov.com.au/news/getting-summer-ready-december-2019
  - 404 Not found https://www.ewov.com.au/uploads/main/EWOV-submission-to-Embedded-Networks-Review-Issues-Paper-March-2021.pdf
    Linked from: https://www.ewov.com.au/reports/reflect-202105
