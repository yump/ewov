# Maintenance Log

## Important Notes (Always stick to the top please)

### Maintenace report Mar 2024 - Alice
* Updated Craft and all plugins to the latest version on both staging and production:
    - craft 4.12.8 => 4.14.10
    - blitz 4.23.3 => 4.23.12
    - freeform 4.1.22 => 4.1.24
    - imager X 4.4.1 => 4.5.1.1
    - neo 4.2.18 => 4.4.0
    - retour 4.1.19 => 4.1.24
    - seomatic 4.1.5 => 4.1.11
    - tablemaker 4.0.14 => 4.0.16
* cPanel:
  - error logs - all good
  - File Usage - 104,110 / 500,000 (20.82%)
  - Disk Usage: 9.88 GB / 50 GB (19.75%)
  - Memory Usage: 201.29 MB / 8 GB (2.46%)
  - Mysql Disk Usage: 1.34 GB / 41.46 GB (3.23%)
  - Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
* Form submission, Email notification - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Broken links:

404 Not found	
https://www.ewov.com.au/news/greater-western-water-bill-delay-old
Linked from: https://www.ewov.com.au/common-complaints/delayed-and-catch-up-bills
Link text: news page

404 Not found	
https://www.cleanenergycouncil.org.au/cec.html
Linked from: https://www.ewov.com.au/about-us/faqs
Link text: Clean Energy Council

404 Not found	
https://www.ausnetservices.com.au/about/community/energy-resilience-community-fund
Linked from: https://www.ewov.com.au/reports/reflect-202405
Link text: apply for a hardship grant

404 Not found	
https://www.ewov.com.au/data-and-reports/data-hub/quarterly-case-data-v3
Linked from: 
  - https://www.ewov.com.au/reports/reflect-202305
  - https://www.ewov.com.au/reports/reflect-202303
  - https://www.ewov.com.au/reports/reflect-202208
  - https://www.ewov.com.au/reports/reflect-202211
  - https://www.ewov.com.au/reports/reflect-202202
  - https://www.ewov.com.au/reports/reflect-202205-2
  - https://www.ewov.com.au/reports/reflect-202110
  - https://www.ewov.com.au/reports/reflect-202108
  - https://www.ewov.com.au/reports/reflect-202105
  - https://www.ewov.com.au/reports/reflect-202103
Link text: Data Hub

404 Not found	
https://www.ewov.com.au/uploads/main/EWOV-Draft-Family-Violence-Position-Statement.pdf
Linked from: https://www.ewov.com.au/reports/empowering-change
Link source: The family violence position statement image.

404 Not found	
https://www.ewov.com.au/fact-sheets/land-access-preliminary-assessment-v3
Linked from: https://www.ewov.com.au/fact-sheets/land-access-investigation-stage
Link text: Land access preliminary assessment fact sheet

Host not found	
https://recovery.serviceconnect.gov.au/<a href>Outbound
Linked from: https://www.ewov.com.au/news/natural-disaster-support
Link text: 
  - Recovery Connect
  - Recovery Connect -> Go to service

### Maintenace report Oct 2024 - Alice
* Updated Craft and all plugins to the latest version on both staging and production:
    - craft 4.11.2 => 4.12.8
    - blitz 4.22.0 => 4.23.3
    - freeform 4.1.20 => 4.1.22
    - neo 4.2.10 => 4.2.18
    - retour 4.1.18 => 4.1.19
    - seomatic 4.1.1 => 4.1.5
    - tablemaker 4.0.12 => 4.0.14
    - [Plugin] the plugin "sebastianlenz/linkfield" is no longer maintaineed and should be replaced with "verbb/hyper" plugin. Estimated time to change: (1 hours) + plugin cost $19 USD onetime ($9 USD annually for renew)
* cPanel:
  - error logs - all good
  - File Usage - 95,560 / 500,000 (19.11%)
  - Disk Usage: 9.66 GB / 50 GB (19.32%)
  - Memory Usage: 474.99 MB / 8 GB (5.8%)
  - Mysql Disk Usage: 1.32 GB / 41.66 GB (3.18%)
  - Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
* Form submission, Email notification - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Broken links:
404 Not found	
https://www.cleanenergycouncil.org.au/cec.html
Linked from: https://www.ewov.com.au/about-us/faqs

404 Not found	
https://www.ewov.com.au/news/ses,vic.gov.au
Linked from: https://www.ewov.com.au/news/natural-disaster-support

404 Not found	
https://www.ausnetservices.com.au/about/community/energy-resilience-community-fund
Linked from: https://www.ewov.com.au/reports/reflect-202405

Host not found	
mailto:careers@ewov.com.au by
Linked from: https://www.ewov.com.au/about-us/careers/data-engineer

### Maintenace report August 2024 - Alice
* Updated Craft and all plugins to the latest version on both staging and production:
    - craft 4.9.7 => 4.11.2
    - blitz 4.18.1 => 4.22.0
    - field-manager 3.0.8 => 3.0.9
    - freeform 4.1.19 => 4.1.20
    - imager-x 4.4.0 => 4.4.1
    - neo 4.2.1 => 4.2.10
    - redactor 3.0.4 => 3.1.0
    - retour 4.1.16 => 4.1.18
    - seomatic 4.0.49 => 4.1.1
    - tablemaker 4.0.11 => 4.0.12
    - [Plugin] the plugin "sebastianlenz/linkfield" is no longer maintaineed and should be replaced with "verbb/hyper" plugin. Estimated time to change: (1 hours) + plugin cost $19 USD onetime ($9 USD annually for renew)

* cPanel:
  - error logs - all good
  - File Usage - 138,663 / 500,000 (27.73%)
  - Disk Usage: 8.7 GB / 50 GB (17.41%)
  - Memory Usage: 112.53 MB / 8 GB (1.37%)
  - Mysql Disk Usage: 1.3 GB / 42.59 GB (3.04%)
  - Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
* Form submission, Email notification - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Broken links:
Timeout	
https://www.peopleenergy.com.au/
Linked from: https://www.ewov.com.au/members/find-a-member

Host not found	
http://www.qenergy.com.au/
Linked from: https://www.ewov.com.au/members/find-a-member

404 Not found	
https://www.telstra.com.au/energy
Linked from: https://www.ewov.com.au/members/find-a-member

404 Not found	
https://www.ewov.com.au/news/ses,vic.gov.au
Linked from: https://www.ewov.com.au/news/natural-disaster-support

404 Not found	
https://my.gov.au/en/services/living-arrangements/experiencing-a-natural-disaster/getting-help-after-a-natural-disaster/australian-government-disaster-payments/australian-government-disaster-recovery-payment
Linked from: https://www.ewov.com.au/news/natural-disaster-support

404 Not found	
https://www.ewov.com.au/data-and-reports/data-hub/quarterly-case-data
Linked from: 
  - https://www.ewov.com.au/reports/reflect-202305
  - https://www.ewov.com.au/reports/reflect-202303
  - https://www.ewov.com.au/reports/reflect-202211
  - https://www.ewov.com.au/reports/reflect-202208
  - https://www.ewov.com.au/reports/reflect-202205-2
  - https://www.ewov.com.au/reports/reflect-202202
  - https://www.ewov.com.au/reports/ewov-news-sept-2021
  - https://www.ewov.com.au/reports/reflect-202110
  - https://www.ewov.com.au/reports/reflect-202108
  - https://www.ewov.com.au/reports/reflect-202105

404 Not found	
https://www.ewov.com.au/reports/detect-september-2021
Linked from: https://www.ewov.com.au/reports/ewov-news-dec-2021

404 Not found	
https://www.ewov.com.au/reports/detect-may-2021
Linked from: https://www.ewov.com.au/reports/ewov-news-june-2021

404 Not found	
https://www.cleanenergycouncil.org.au/consumers/buying-solar/find-an-installer
Linked from: https://www.ewov.com.au/fact-sheets/solar-faqs

### Maintenace report May 2024
* Updated Craft and all plugins to the latest version on both staging and production:
    - craft 4.9.1 => 4.9.7
    - blitz 4.17.0 => 4.18.1
    - freeform 4.1.17 => 4.1.19
    - neo 4.1.2 => 4.2.1
    - seomatic 4.0.48 => 4.0.49

* cPanel:
  - error logs - all good
  - File Usage - 138,663 / 500,000 (27.73%)
  - Disk Usage: 8.7 GB / 50 GB (17.41%)
  - Memory Usage: 112.53 MB / 8 GB (1.37%)
  - Mysql Disk Usage: 1.3 GB / 42.59 GB (3.04%)
  - Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
* Form submission, Email notification - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Broken links:
http://cris.crisisservices.org.au/
error code: 12007 (no such host), linked from page(s):
https://www.ewov.com.au/about-us/faqs
Link Text:Crisis Referral Information System (under Affordability accordion)

http://www.basslink.com.au/
error code: 500 (server error), linked from page(s):
https://www.ewov.com.au/members/find-a-member
Link Text: Basslink

https://electricityinabox.com.au/
error code: 500 (server error), linked from page(s):
https://www.ewov.com.au/members/find-a-member
Link Text: Electricity in a Box

https://my.gov.au/en/services/living-arrangements/experiencing-a-natural-disaster/getting-help-after-a-natural-disaster/australian-government-disaster-payments/australian-government-disaster-recovery-payment
error code: 404 (not found), linked from page(s):
https://www.ewov.com.au/news/natural-disaster-support
Link Text: Australian Government Disaster Recovery Payment

https://www.cogentenergysystems.com/
error code: 12029 (no connection), linked from page(s):
https://www.ewov.com.au/members/find-a-member
Link Text: Cogent Energy

### Maintenance report Feb 2024
* Updated Craft and all plugins to the latest version on both staging and production:
    - craft 3.9.5 => 3.9.10
    - blitz 3.12.10 => 3.14.0
    - freeform 3.13.32 => 3.13.34
    - neo 2.13.19 => 2.13.20
    - retour 3.2.12 => 3.2.13
    - seomatic 3.4.63 => 3.4.72

* cPanel:
  - error logs - all good
  - File Usage - 82,263 / 500,000 (16.45%)
  - Disk Usage: 7.95 GB / 50 GB (15.91%)
  - Memory Usage: 1.09 GB / 8 GB (13.65%)
  - Mysql Disk Usage: 1.09 GB / 8 GB (13.65%)
  - Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Broken links: 
404 Not found	
https://www.water.vic.gov.au/planning/victorias-entitlement-framework/the-water-act
Linked from: 
https://memport.ewov.com.au/joining-ewov/

404 Not found	
https://www.ewov.com.au/news/ses,vic.gov.au
Linked from: https://www.ewov.com.au/news/natural-disaster-support

404 Not found	
https://jemena.com.au/supply-interruptions
Linked from: https://www.ewov.com.au/fact-sheets/planned-and-unplanned-outages

## Maintenance report for Nov 2023
* Updated Craft and all plugins to the latest version on both staging and production:
  - Craft 3.8.17 => 3.9.5
  - imager-x v3.6.7 => 3.6.8
  - retour 3.2.11 => 3.2.12
  - SEOMatic 3.4.59 => 3.4.63

* cPanel:
  - File Usage - 140,884 / 500,000 (28.18%) - all good
  - Disk Usage: 10.24 GB / 50 GB (20.47%) - all good
  - Memory Usage: 114.37 MB / 8 GB (1.4%) - all good
  - Mysql Disk Usage: 820.25 MB / 41.02 GB (1.97%) - all good
  - error logs - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
 - News, Fact Sheets, Fact Sheet detail page, print, Reports, Search function, Share feature, Form Submissions - all good
* Client may need to inspect this: case snapshot on page: https://www.ewov.com.au/data-and-reports/data-hub/case-snapshot display empty figure.
* Broken links: 
404 not found:
Link: https://www.infrastructure.gov.au/what-we-do/phone/services-people-disability/accesshub/national-relay-service/service-features/national-relay-service-call-numbers
Link From: 
 - https://www.ewov.com.au/contact-us => 'National Relay Service'
 - https://www.ewov.com.au/start-a-complaint => 'National Relay Service'

404 Not Found:
Link: https://www.cisvic.org.au/getting-help/cisvic-model
Link From: https://www.ewov.com.au/about-us/faqs => 'Affordability' => 'Community Information & support VIC' => 'CISVic'

404 Not Found:
Link: https://www.powercor.com.au/outages/outage-map/
Link From: https://www.ewov.com.au/news/planned-and-unplanned-electricity-supply-interruptions-during-lockdowns-september-2021 => 'Citipower and Powercor'

Timeout:
Link: https://www.cogentenergysystems.com/
Link From: https://www.ewov.com.au/members/find-a-member => 'Electricity retail' => 'Cogent Energy'

Timeout:
Link: https://elysianenergy.com.au/
Link From: https://www.ewov.com.au/members/find-a-member => 'Electricity retail' => 'Elysian Energy'

Timeout:
Link: https://www.peopleenergy.com.au/
Link From: https://www.ewov.com.au/members/find-a-member => 'Electricity retail' => 'People Energy'




## Maintenance report for Aug 2023
* Updated Craft and all plugins to the latest version on both staging and production:
  - craft 3.8.12 => 3.8.17
  - blitz 3.12.9 => 3.12.10
  - freeform 3.13.29 => 3.13.32
  - imager-x v3.6.6 => v3.6.7
  - neo 2.13.18 => 2.13.19
  - retour 3.2.10 => 3.2.11
  - seomatic 3.4.54 => 3.4.59
* cPanel: 
  - File Usage - 139,290 / 500,000 (27.86%) - all good
  - Disk Usage: 9.75 GB / 50 GB (19.5%) - all good
  - Memory Usage: 124.21 MB / 8 GB (1.52%) - all good
  - Mysql Disk Usage: 795.05 MB / 41.02 GB (1.89%) - all good
  - error logs - all good
* Google search console - no errors
* Google Analytics data collection checked - all good
* Daily backup (Acorns Backup) - all good
* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
 - News, Fact Sheets, Fact Sheet detail page, print, Reports, Search function, Share feature, Form Submissions - all good
* Broken links: 
Timeout	
https://www.cogentenergysystems.com/
Linked from: https://www.ewov.com.au/members/find-a-member
Timeout	
https://elysianenergy.com.au/
Linked from: https://www.ewov.com.au/members/find-a-member
Timeout	
https://www.peopleenergy.com.au/
Linked from: https://www.ewov.com.au/members/find-a-member
404 Not found	
https://www.communications.gov.au/what-we-do/phone/services-people-disability/accesshub/national-relay-service/service-features/national-relay-service-call-numbers
Linked from
https://www.ewov.com.au/contact-us
https://www.ewov.com.au/start-a-complaint
https://www.ewov.com.au/start-a-complaint

404 Not found	
https://www.cisvic.org.au/getting-help/cisvic-model
Linked from: https://www.ewov.com.au/about-us/faqs

## Maintenance report for 24 May 2023
* Updated Craft and all plugins to the latest version on both staging and production:
 - Craft 3.7.65 => 3.8.12
- redactor 2.10.11 => 2.10.12
 - Retour 3.2.7 => 3.2.10
 - Seomatic 3.4.48 => 3.4.54
 - Blitz 3.12.8 => 3.12.9
 - Freeform 3.13.22.1 => 3.13.29
 - Imager-x v3.6.5 => v3.6.6
 - Neo 2.13.16 => 2.13.18

* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages - all good
 - News, Fact Sheets, Fact Sheet detail page, print - all good
 - Reports - all good
 - Search function - all good
 - Share feature - all good
 - Form Submissions - all good

* cPanel: File Usage - 139,306 / 500,000 (27.86%) - all good
* cPanel: Disk Usage: 9.69 GB / 50 GB (19.38%) - all good
* cPanel: Memory Usage: 108.53 MB / 8 GB (1.32%) - all good
* cPanel: Mysql Disk Usage: 786.75 MB / 41.08 GB (1.87%) - all good
* cPanel: error logs - all good
* Google search console - all good
* Google Analytics data collection checked - all good
* Daily backup (Acorns Backup) - all good
* Broken links: 

https://www.accc.gov.au/business/unfair-business-practices/unconscionable-conduct
Linked from page:
https://www.ewov.com.au/fact-sheets/credit-default-listings-and-debt-collection
Link Text: accc.gov.au/business/unfair-business-practices/unconscionable-conduct

https://www.energy.vic.gov.au/legislation/victorian-lpg-retail-code
Linked from page:
	https://www.ewov.com.au/fact-sheets/ewov-for-mps
Link Text: energy.vic.gov.au/legislation/victorian-lpg-retail-code

https://www.ewov.com.au/about-us/careers/junior-data-engineer
Linked from page:
	https://www.ewov.com.au/about-us/careers
Link on icon (right side of “Independent Technical Panel Members”)

https://www.smartwatermark.org/Victoria/
Linked from page:
	https://www.ewov.com.au/news/getting-summer-ready-december-2019
Link Text: Smart Approved WaterMark

https://www.sustainability.vic.gov.au/energy-efficiency-and-reducing-emissions/save-energy-in-the-home/reduce-heating-costs-at-home
Linked from page:
	https://www.ewov.com.au/news/seasonality-and-utility-bills-june-2022
Link Text: Sustainability Victoria website


### 8th Feb 2023
* Database auto backup is on
* Sync staging and production content
* Backup production assets and databases into staging
* Updated Craft and all plugins to the latest version on both staging and production:
  - craft 3.7.59 => 3.7.61
  - redactor 2.10.10 => 2.11.11
  - retour 3.2.3 => 3.2.7
  - seomatic 3.4.42 => 3.4.48
  - blitz 3.12.6 => 3.12.8
  - freeform 3.13.21 => 3.13.22.1
  - neo 2.13.15 => 2.13.16
  - trendyminds/visor removed due to deprecation
  - add yump visor module to replace trendyminds/visor

* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages (news, fact sheets, form submission) - all look good
* cPanel: file usage - 129,944 / 500,000 (25.99%)  - All good
* cPanel: disk usage - 5.15 GB / 25 GB (20.6%) - All good
* cPanel: mysql disk usage - 728.11 MB / 42.19 GB (1.69%) -- all good
* cPanel: error logs - all good
* Google search console - all good
* Google Analytics data collection - all good
* Broken links:
- https://www.ewov.com.au/uploads/main/EWOV-submission-to-Embedded-Networks-Review-Issues-Paper-March-2021.pdf <a href>
  Linked from: https://www.ewov.com.au/reports/reflect-202208
- https://ewov.yump.com.au/start-a-complaint 
  Linked from: https://www.ewov.com.au/common-complaints/other-issues
- https://www.energy.vic.gov.au/safety-and-emergencies/power-outages/customer-compensation 
  Linked from: https://www.ewov.com.au/fact-sheets/planned-and-unplanned-outages and https://www.ewov.com.au/fact-sheets/voltage-variations-and-power-surges
- https://www.energy.vic.gov.au/__data/assets/pdf_file/0025/504763/Prepare-for-power-outages-brochure-2020.pdf 
  Linked from: https://www.ewov.com.au/fact-sheets/planned-and-unplanned-outages
- https://esv.vic.gov.au/news/electricity-hazards-and-safety-guide-published/ 
  Linked from: https://www.ewov.com.au/fact-sheets/planned-and-unplanned-outages

### 10th August 2022

* Database auto backup is on
* Updated Craft and all plugins to the latest version on both staging and production:

  - craft 3.7.39 => 3.7.51
  - redactor 2.10.7 => 2.10.10
  - retour 3.1.71 => 3.1.73
  - seomatic 3.4.30 => 3.4.37
  - blitz 3.12.5 => 3.12.15
  - freeform 3.13.8 => 3.13.17
  - imager-x v3.5.8 => v3.6.2
  - neo 2.13.6 => 2.13.15
  - field-manager 2.2.4 => 2.2.5
  - supercool/tablemaker 2.0.1 => verbb/tablemaker 3.0.1


* CMS: assets upload, all entries, all templates, SEO - all good
* Front end: all templates and pages (news, fact sheets, form submission) - all look good
* cPanel:  file usage - 153,311 / 500,000 (30.66%)  - All good
* cPanel: error logs - all good
* Google search console - all good
* Google Analytics data collection - all good
* Broken links:
Too many redirects	
https://1stenergy.com.au/
Linked from: https://www.ewov.com.au/members/find-a-member

404 Not found	
http://www.anzoa.com.au/about-ombudsmen.html
Linked from: https://www.ewov.com.au/about-us/who-we-are

### 25 May 2020 - By Fei 
* Backed up database in local environment
* Updated Craft and all plugins to the latest version
* Updated staging site up to date
* Fix all deprecation issues
* Check disk usage - 5.15 GB / 25 GB (20.6%) - All good
* Check error log - all good
* Enable server cache
* Fix performance issue for some page
* Xmenu check broken link - all good
