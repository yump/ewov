<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\jobs;

use modules\yumpmodule\YumpModule;

use Craft;
use craft\queue\BaseJob;

/**
 * Refresh nav and main nav cache
 *
 * More info: https://github.com/yiisoft/yii2-queue
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class NavRefresher extends BaseJob
{
    // Public Properties
    // =========================================================================

    // Public Methods
    // =========================================================================

    /**
     * When the Queue is ready to run your job, it will call this method.
     * You don't need any steps or any other special logic handling, just do the
     * jobs that needs to be done here.
     *
     * More info: https://github.com/yiisoft/yii2-queue
     */
    public function execute($queue): void
    {
        YumpModule::$instance->yump->forceUpdateNav(); // update site nav
        // Craft::info('Executing job: forceUpdateNav()'); // this doesn't work for some reasons

        YumpModule::$instance->yump->forceUpdateNav(true); // update main nav
    }

    // Protected Methods
    // =========================================================================

    /**
     * Returns a default description for [[getDescription()]], if [[description]] isn’t set.
     *
     * @return string The default task description
     */
    protected function defaultDescription(): string
    {
        return 'Refresh Nav and MainNav';
    }
}
