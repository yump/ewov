<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\services;

use modules\yumpmodule\YumpModule;
use modules\yumpmodule\helpers\YumpCurlHelper;

use Craft;
use craft\base\Component;

/**
 * YumpModuleBlackboxService Service
 *
 * This service is separate from YumpModuleService, which is directly connected with YumpModuleVariable. So if we call a function from Twig like this: craft.yump.exampleService, we will look for function with the same name in YumpModuleService, if that's missing in YumpModuleVariable. However, sometimes we don't want devs to accidentally call a function in YumpModuleService which make an curl request, or do something with the database. This is when this Blackbox service comes from. It means it can only be accessed by PHP files. This is kinda the same concept as separating Variable from Service, but one downside of variable is that PHP cannot access functions in Variable. That's why we have the general service (YumpModuleService), which can be access from both Twig and PHP.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class YumpModuleBlackboxService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     YumpModule::$instance->blackbox->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'I am a black box.';

        return $result;
    }

    public function curl($url, $options = []) {
        // if using curl cache, please add option: 'cacheFolder' => CRAFT_BASE_PATH . "/yump/private/cache/curl"

        return YumpCurlHelper::curl($url, $options);
    }

}
