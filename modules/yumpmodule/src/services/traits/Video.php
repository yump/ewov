<?php
namespace modules\yumpmodule\services\traits;

trait Video
{
    /**
     * compile the video (youtube/vimeo) URL, given the video URL and the params
     * @param  string $url  video URL, assuming we've already have the URL sanitised (removed the whitespaces)
     * @param  array  $params URL params you want to add to the URL. YouTube and Vimeo have their own list of params you can refer to.
     * - YouTube: https://developers.google.com/youtube/player_parameters#Parameters
     * - Vimeo: https://vimeo.zendesk.com/hc/en-us/articles/360001494447-Player-parameters-overview
     * @return string|null         The compiled URL
     */
    public function compileVideoUrl(string $url, array $params = []):?string {
        if($this->isYouTube($url) || $this->isVimeo($url)) {
            $parameters = '';

            // check if there are any parameters passed along
            if (!empty($params)) {

                $parameters .= '?';
                $i = 0;
                foreach ($params as $k=>$v) {
                    if (($parameters !== null) && ($i !== 0)) {
                        $parameters .= '&';
                    }
                    $parameters .= "{$k}={$v}";
                    $i++;
                }
            }

            if ($this->isYouTube($url)) {
                $id = $this->getYouTubeId($url);

                return '//www.youtube.com/embed/' . $id . $parameters;
//                return $embedUrl;
            } else if ($this->isVimeo($url)) {
                $id = $this->getVimeoId($url);

                return '//player.vimeo.com/video/' . $id . $parameters;
            }
        }

        return null;
        // else (if not YouTube or Vimeo), return null
    }

    /**
     * Is the url a youtube url
     * @param string $url
     * @return boolean
     */
    private function isYouTube(string $url):bool {
        return str_contains($url, 'youtube.com') || str_contains($url, 'youtu.be');
    }

    /**
     * Is the url a vimeo url
     * @param string $url
     * @return boolean
     */
    private function isVimeo(string $url):bool
    {
        return str_contains($url, 'vimeo.com');
    }

    /**
     * Parse the YouTube URL, return the video ID
     * @param string $url
     * @return string
     */
    private function getYouTubeId(string $url):string
    {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        return $match[1];
    }

    /**
     * Parse the Vimeo URL, return the video ID
     * @param string $url
     * @return string
     */
    private function getVimeoId(string $url):string
    {
        preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $matches);
        return $matches[3];
    }

    /** ** The following functions are copied from VideoEmbedder plugin, we will probably never use them, but keep them for our references. ** */

    /**
     * Is the url a wistia url
     * @param string $url
     * @return boolean
     */
    private function isWistia(string $url):bool
    {
        return str_contains($url, 'wistia.com/');
    }

    /**
     * Is the url a Viddler url
     * @param string $url
     * @return boolean
     */
    private function isViddler(string $url):bool
    {
        return str_contains($url, 'viddler.com/');
    }

    /**
     * Is the url using oEmbed format
     * @param string $url
     * @return boolean
     */
    private function isOembed(string $url):bool
    {
        return str_contains($url, 'oembed.json?');
    }

    /** ** End of the other code copied from VideoEmbedder plugin ** */
}
