<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\services;

use modules\yumpmodule\YumpModule;

use Craft;
use craft\base\Component;
use craft\helpers\ConfigHelper;

/**
 * YumpModuleCacheService Service
 *
 * This service is not directly accessible via Twig.
 *
 * This service is for managing cache in the following two cache methods:
 * - Craft built-in cache
 * - Yump custom cache
 *
 * The biggest different between the two cache methods are:
 * 
 * Craft built-in cache:
 * ---
 * - It's stored in database
 * - It uses Craft's built-in cache mechanism, so it can be flushed via the CP.
 * - Cache expiry is defined while SETTING or UPDATING the cache.
 * - Cache is automatically expired based on the expiry setting while setting / updating.
 *
 * Yump custom cache:
 * - It's stored in physical folders. 
 * ..- By default it's under @root/yump/private/cache/data
 * ..- There's a setting which you can set it to the public folder, which is @root/yump/public/cache, so the cached file can be publicly accessible. 
 * ..- Or, there's another option which you can define the cache location wherever you want. Reminder to create the folder, otherwise the cache won't work! Recommend putting an empty file under the folder and commit it to repo so it won't get lost in staging / production.
 * - There's no expiry while setting / updating the cache, but there's an expiry setting while GETTING the cache. It basically determine that we will only get the content if the file was created within the expiry time.
 * - The cache can be manually removed via FTP if you want to flush it.
 * - This cache DOES NOT automatically expired. So if nothing triggers an update, it will always show the last updated content, no matter how old it was.
 *
 * We usually need to code a way to update the cache, no matter which cache method you chose, unless you feel it is fine to solely rely on the expiry setting of the cache.
 *
 * Both can do
 * ---
 * They both can do things like the followings:
 * - Having a cron job keep updating the cache
 * - Get content -> if cached and not expired, get from cache -> otherwise, get fresh copy -> and also set new cache
 *
 * These both need to be coded separately though.
 *
 * So it is totally up to you which one you want to use, depending on different situations and what's the best suitable solution for the client, as long as you understand all the differences between the two.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class YumpModuleCacheService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     YumpModule::$instance->cache->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'I am the cache service.';

        return $result;
    }

    /**
     * Get cache content from Craft cache
     * 
     * @param  string  $cacheKey the cache content identifier.
     * @param  boolean $returnAsArray by default, this will return the content (usually in JSON format); if set to true, it will return PHP array (only work if the content is in valid JSON format)
     * @return [json string / array / null if empty content]
     */
    public function getCraftCachedContent($cacheKey, $returnAsArray = false) {
        return $this->getCachedContent($cacheKey, $returnAsArray);
    }

    /**
     * Get cache content from Yump custom cache
     * 
     * @param  string  $cacheKey the cache content identifier.
     * @param  boolean $returnAsArray by default, this will return the content (usually in JSON format); if set to true, it will return PHP array (only work if the content is in valid JSON format)
     * @param  array $yumpCacheOptions defines custom options for Yump Cache
     * Available Options:
     * - 1. expiry (false|int):
     *   - false (default): turn off the expiry, so we can get the content as long as the cache file exists.
     *   - int: number of seconds
     * - 2. usePublicFolder (boolean): Instead of using the default yump cache folder, some cache files might be located under the public web folder, so we can access it directly on front end. If this is set to true, then we will look at the default public cache folder: CRAFT_BASE_PATH . "/yump/public/cache".
     * - 3. customFolder (string - folder path): regardless of the default public or non-public cache folder, this option will override the cache folder, if the cache is located somewhere else. IMPORTANT: reminder to create the folder, otherwise the cache won't work! Recommend putting an empty file under the folder and commit it to repo so it won't get lost in staging / production.
     * @return [json string / array / null if empty content]
     */
    public function getYumpCachedContent($cacheKey, $returnAsArray = false, $yumpCacheOptions = array()) {
        return $this->getCachedContent($cacheKey, $returnAsArray, true, $yumpCacheOptions);
    }

    /**
     * Get cache content from either Craft or Yump custom cache
     * 
     * NOTE: Usually use the two functions above, instead of this one. This function joins the two together, so we can reduce certain amount of duplicated code. But this is not very logical, because they do have different settings.
     * 
     * @param  string  $cacheKey the cache content identifier.
     * @param  boolean $returnAsArray by default, this will return the content (usually in JSON format); if set to true, it will return PHP array (only work if the content is in valid JSON format)
     * @param  boolean $useYumpCache by default, we try to get cache from Craft (using Craft's built-in cache functions); if this is set to true, we will get cache from Yump's cache folder.
     * @param  array $yumpCacheOptions defines custom options for Yump Cache
     * Available Options:
     * - 1. expiry (false|int): WISHLIST - allow using Craft default config cacheDuration by setting this to true, or use a interval string.
     *   - false (default): turn off the expiry, so we can get the content as long as the cache file exists.
     *   - int: number of seconds
     * - 2. usePublicFolder (boolean): Instead of using the default yump cache folder, some cache files might be located under the public web folder, so we can access it directly on front end. If this is set to true, then we will look at the default public cache folder: CRAFT_BASE_PATH . "/yump/public/cache".
     * - 3. customFolder (string - folder path): regardless of the default public or non-public cache folder, this option will override the cache folder, if the cache is located somewhere else. IMPORTANT: reminder to create the folder, otherwise the cache won't work! Recommend putting an empty file under the folder and commit it to repo so it won't get lost in staging / production.
     * @return [json string / array / null if empty content]
     */
    public function getCachedContent($cacheKey, $returnAsArray = false, $useYumpCache = false, $yumpCacheOptions = array()) {
        $cachedContent = null;

        try {
            /////// Retrieving cache content ////////////
            if(!$useYumpCache) {
                // using Craft's built-in cache
                $cacheService = Craft::$app->getCache();
                $cachedContent = $cacheService->get($cacheKey);
            } else {
                // using Yump's custom cache
                $filePath = $this->_getYumpCacheFolder($yumpCacheOptions) . "/" . $cacheKey . ".json";
                $expiry = !empty($yumpCacheOptions['expiry']) ? $yumpCacheOptions['expiry'] : false;
                if(
                    file_exists($filePath) // file exists
                    and
                    (
                        $expiry === false // expiry is disabled
                        or
                        filemtime($filePath) >= strtotime("-" . $expiry . " second" . ($expiry !== 1 ? "s" : "")) // hasn't expired
                    )
                ) {
                    $cachedContent = @file_get_contents($filePath);
                } else {
                    Craft::warning("Cannot get the cache content because the file: '" . $filePath . "' does not exist or expired!" . " Cache key: \"" . $cacheKey . " Expiry: " . $expiry . " seconds", __METHOD__);
                }
            }

            /////// Now we have the content /////////

            // if the content is not empty
            if($cachedContent !== false and $cachedContent !== null) {
                // if return as array instead of JSON
                if($returnAsArray) {
                    return YumpModule::$instance->yump->yump_json_decode($cachedContent, "Failed to json_decode the data from Craft cache content.");
                }
                // if return as it is (string. Usually a JSON string)
                else {
                    return $cachedContent;
                }
            } else {
                Craft::info("Retrieved empty content from Cache. The cache content does not exist, or was expired. Cache key: \"" . $cacheKey, __METHOD__);
            }
        } catch (\Exception $e) {
            Craft::error($e->getMessage() . "\r\n-----\r\nCache key: \"" . $cacheKey . "\"." 
                // . " Cache content was: " . $cachedContent
                , __METHOD__
            );
            Craft::debug(
                "at Line " . $e->getLine() . 
                "\r\n" . 
                $e->getTraceAsString(),
                __METHOD__
            );
        }
    }

    /**
     * Set cache content using Craft's built-in cache
     * 
     * @param string $cacheKey
     * @param string $content
     * @param false|int|string $customExpiry indicates how long this cache would expire. TODO: this seems a bit buggy, if you set custom expiry, and then change it to false, it doesn't seem to respect the cacheDuration config. But I guess we don't normally need to change the expiry, unless in development?
     * false: use Craft's cacheDuration config
     * int: number of seconds
     * string: a interval spec string (see: http://www.php.net/manual/en/dateinterval.construct.php)
     * @return  boolean indicates success or fail
     */
    public function setCraftCachedContent($cacheKey, $content, $customExpiry = false) {
        if ($customExpiry !== false) {
            $expire = ConfigHelper::durationInSeconds($customExpiry);
        } else {
            $expire = null;
        }

        $cacheService = Craft::$app->getCache();
        if(($cacheService->set($cacheKey, $content, $expire)) === FALSE) {
            Craft::warning("Could not save the cache content to Craft cache so the cache was not updated." . " Cache key: \"" . $cacheKey . " Expiry (in seconds): " . $expire, __METHOD__);
        } else {
            Craft::info("Cache successfully set / updated! " . " Cache key: \"" . $cacheKey . " Expiry (in seconds): " . $expire, __METHOD__);
            return true;
        }

        return false;
    }

    /**
     * Set cache content using Yump's custom cache method.
     *
     * See getYumpCachedContent or getCachedContent for the explanation of the options.
     * @param [type] $cacheKey         [description]
     * @param [type] $content          [description]
     * @param array  $yumpCacheOptions [description]
     * @return  boolean indicates success or fail
     */
    public function setYumpCachedContent($cacheKey, $content, $yumpCacheOptions = array()) {
        $yumpCacheFolder = $this->_getYumpCacheFolder($yumpCacheOptions);
        $filePath = $yumpCacheFolder . "/" . $cacheKey . ".json";

        if(file_exists($yumpCacheFolder)) {
            if((file_put_contents($filePath, $content, LOCK_EX)) === FALSE) {
                Craft::warning("Could not save the cache content to Yump cache filePath '" . $filePath . "' so the cache was not updated." . " Cache key: \"" . $cacheKey, __METHOD__);
            } else {
                Craft::info("Cache successfully set / updated! File path: '" . $filePath, __METHOD__);
                return true;
            }
        } else {
            Craft::warning("Could not find the Yump cache folder '" . $yumpCacheFolder . "' so the cache was not updated." . " Cache key: \"" . $cacheKey, __METHOD__);
        }

        return false;
    }

    private function _getYumpCacheFolder($yumpCacheOptions = array()) {
        $yumpCacheFolder = CRAFT_BASE_PATH . "/yump/private/cache/data";
        if(!empty($yumpCacheOptions['usePublicFolder'])) {
            $yumpCacheFolder = CRAFT_BASE_PATH . "/yump/public/cache";
        }
        if(!empty($yumpCacheOptions['customFolder'])) {
            $yumpCacheFolder = $yumpCacheOptions['customFolder'];
        }

        return $yumpCacheFolder;
    }
}
