<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\services;

use craft\elements\Entry;
use craft\elements\Asset;
use craft\elements\db\ElementQuery;
use modules\yumpmodule\YumpModule;

use Craft;
use craft\base\Component;

use spacecatninja\imagerx\ImagerX;
//use aelvan\imager\Imager;
use modules\yumpmodule\gears\cache\adapters\Nav as YumpNavAdapter;
use modules\yumpmodule\gears\cache\adapters\MainNav as YumpMainNavAdapter;
use craft\helpers\ElementHelper;

/**
 * YumpModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * This class is directly connected with YumpModuleVariable. So if we call a function from Twig like this: craft.yump.exampleService, we will look for function with the same name in YumpModuleService, if that's missing in YumpModuleVariable. However, sometimes we don't want devs to accidentally call a function in YumpModuleService which make an curl request, or do something with the database. This is when this YumpModuleBlackboxService comes from. It means it can only be accessed by PHP files. This is kinda the same concept as separating Variable from Service, but one downside of variable is that PHP cannot access functions in Variable. That's why we have the general service (YumpModuleService), which can be access from both Twig and PHP.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class YumpModuleService extends Component
{
    use traits\Video;
    private $_cachedNavPhpArray;
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     YumpModule::$instance->yump->exampleService()
     *
     * From Twig template to access functions in this file: craft.yump.exampleService()
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }

    public function getIOSDevice()
    {
        $device = false;
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Macintosh') !== false) {
                $device = 'mac';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false) {
                $device = 'iPhone';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
                $device = 'iPad';
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPod') !== false) {
                $device = 'iPod';
            }
        }

        return $device;
    }

    public function ieVersion()
    {
        $version = false;
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false) {
                $version = 6;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') !== false) {
                $version = 7;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.') !== false) {
                $version = 8;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !== false) {
                $version = 9;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10.') !== false) {
                $version = 10;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {
                $version = 11;
            }
        }

        return $version;
    }

    /**
     * get all entries under section (with section handle provided)
     * @param string $sectionHandle
     * @return array of entries
     *
     * Usage example (in PHP): Craft::$app->yump->getAllEntriesBySectionHandle($handle);
     *
     * To use in Twig template, use this:
     * craft.yump.getAllEntriesBySectionHandle($sectionHandle);
     *
     * Or use craft built-in:
     * <% craft.entries.section( $sectionHandle ).find() %>
     */
    public function getAllEntriesBySectionHandle($sectionHandle, $limit = null)
    {
        return craft\elements\Entry::find()
            ->section($sectionHandle)
            ->limit($limit)
            ->all();
    }

    /**
     * NOTE: currently only works for text and textarea, might extend it to other types in the future.
     *
     * @param  [FieldModel] $field
     * @return [string]
     */
    private function _determineFieldType($field)
    {
        if ($field->type == 'PlainText') {
            if (empty($field->settings['multiline'])) {
                return 'text';
            } else {
                return 'textarea';
            }
        }

        return '';
    }

    public function getUserGroupIdByHandle($userGroupHandle)
    {
        $groupId = Craft::$app->userGroups->getGroupByHandle($userGroupHandle)->id;
        return $groupId;
    }

    public function getSectionIdByHandle($sectionHandle)
    {
        $sectionId = Craft::$app->sections->getSectionByHandle($sectionHandle)->id;
        return $sectionId;
    }

    /**
     * Generate validated URL from admin input (e.g. sometimes admins might accidentally put in whitespaces, or forget the protocol)
     * @param  [type] $url [description]
     * @return [type]      [description]
     */
    public function generateValidUrl($url)
    {
        if (!empty($url)) {
            $url = trim($url);

            // excluding url starting with mailto:, tel:, # and /
            if (substr($url, 0, 1) == "/" or substr($url, 0, 1) == '#' or substr($url, 0, 7) == 'mailto:' or substr($url, 0, 4) == 'tel:') {
                return $url;
            }

            if (substr($url, 0, 7) !== "http://" and substr($url, 0, 8) !== "https://") {
                $url = '//' . $url;
            }
            return $url;
        }
    }

    /**
     * Surprised, we get ElementCriteriaModel instead of EntryModel if we call EntriesService->getEntryById in Twig! and the result is wrong, i guess it couldn't find the entry using the ID we supplied, (cuz the passed-in $entryId was tested as null for some reasons). Anyway, I just create this fix function for twig to use.
     * @param  [type] $entryId  [description]
     * @param  [type] $localeId [description]
     * @return [type]           [description]
     */
    public function getEntryByIdFix($entryId, $localeId = null)
    {
        if (is_numeric($entryId)) {
            return Craft::$app->entries->getEntryById((int)$entryId, $localeId);
        }
    }

    /**
     * [getFlashMessage description]
     * @param string $handle default: 'error'; also, 'notice'
     * @return String
     */
    public function getFlashMessage($handle = 'error')
    {
        return Craft::$app->userSession->getFlash($handle);
    }

    public function addHttpSessionVariable($key, $value)
    {
        Craft::$app->httpSession->add($key, $value);
    }

    public function isStaging()
    {
        return strpos($_SERVER['SERVER_NAME'], 'staging');
    }

    public function isDev()
    {
        return strpos($_SERVER['SERVER_NAME'], '.dev') or strpos($_SERVER['SERVER_NAME'], '.test');
    }

    public function isProduction()
    {
        return !$this->isStaging() and !$this->isDev();
    }

    public function getGlobalFieldValue($globalSetHandle, $fieldHandle)
    {
        $settings = Craft::$app->globals->getSetByHandle($globalSetHandle);

        if (!empty($settings)) {
            return $settings->$fieldHandle;
        } else {
            return null;
        }
    }

    /**
     * Get some useful data for server error debugging (e.g. send email notification to devs)
     * @return Array
     */
    public function getHttpRequestData()
    {
        $data = [];
        $requestType = Craft::$app->request->getRequestType();
        if ($requestType == 'GET') {
            $params = $_GET;
        } else if ($requestType == 'POST') {
            $params = $_POST;
        } else {
            $params = Craft::$app->request->getRestParams();
        }
        $isAjax = Craft::$app->request->isAjaxRequest();

        $data['url'] = Craft::$app->request->url;
        $data['requestType'] = $requestType;
        $data['ip'] = Craft::$app->request->getIpAddress();
        $data['hostname'] = gethostbyaddr($data['ip']);
        $data['isAjax'] = $isAjax ? 'Yes' : 'No';
        $data['contentType'] = Craft::$app->request->getMimeType();
        $data['params'] = print_r($params, true);

        $data = array_merge($data, getallheaders());

        $data['sessionVars'] = print_r($_SESSION, true);

        return $data;
    }


    /**
     * get array of values / labels of the value of a multi-choice field
     * @param EntryModel $entry
     * @param string $fieldHandle
     * @param boolean $getAssociatedArray get associated array version of results. If this is set to true, then $getLabel becomes redundant
     * @param boolean $getLabel get array of labels, otherwise, get array of values
     * @return array               array of string
     */
    public function getArrayOfMultiSelectOptions($entry, $fieldHandle, $getAssociatedArray = false, $getLabel = false)
    {
        $result = [];
        foreach ($entry->$fieldHandle as $option) {
            if ($getAssociatedArray) {
                $result[$option->value] = $option->label;
            } else {
                if ($getLabel) {
                    $result[] = $option->label;
                } else {
                    $result[] = $option->value;
                }
            }

        }
        return $result;
    }

    /** return site url with protocol and server name (without back slash at the end) */
    public function getSiteUrl()
    {
        return sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME']
        );
    }

    public function truncate($string, $length = 100, $append = "...")
    {
        $string = trim($string);

        if (strlen($string) > $length) {
            $string = wordwrap($string, $length);
            $string = explode("\n", $string, 2);
            $string = $string[0] . $append;
        }

        return $string;
    }

    /**
     * ***************************************************************************************
     * In PHP, some functions throws notice / errors instead of exceptions.
     * If we DO want to catch those, we can use the following function. However, remember to restore the error handle when its done, no matter success or not.
     * The code to restore error handler: either use the restoreErrorHandler function underneath if can't remember, or use PHP restore_error_handler();
     *
     * Example:
     * set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
     * // error was suppressed with the @-operator
     * if (0 === error_reporting()) {
     * return false;
     * }
     *
     * throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
     * });
     *
     * try {
     * $contentArr = unserialize($content); // the code we want to make sure it wouldn't stop app
     * restore_error_handler();
     * } catch (\Exception $e) {
     * print_r("Things go wrong!");
     * restore_error_handler();
     * }
     */
    public function setErrorHandler()
    {
        set_error_handler(function ($errno, $errstr, $errfile, $errline, array $errcontext) {
            // error was suppressed with the @-operator
            if (0 === error_reporting()) {
                return false;
            }

            throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
    }

    public function restoreErrorHandler()
    {
        restore_error_handler();
    }

    public function getGtmId()
    {
        // new way of getting GTM ID: via SEOMatic settings
        $gtmId = $this->getSiteIdentityAttribute('googleTagManagerID');
        if ($gtmId) return $gtmId;

        // if not set, try to get it from the old way: defined in globals

        // old way of getting GTM IDs
        $matrix = Craft::$app->yump->getGlobalFieldValue('externalScripts', 'externalScripts');

        if ($matrix) {
            foreach ($matrix as $block) {
                if ($block->type == 'googleTagManager' and !empty(trim($block['googleTagManagerId']))) {
                    return trim($block['googleTagManagerId']);
                }
            }
        }
    }

    public function getMicrotimeAsId()
    {
        return str_replace(" ", "-", str_replace(".", "-", microtime()));
    }

    public function getFirstname($fullName)
    {
        $nameArr = explode(' ', $fullName, 2);
        if (count($nameArr) >= 1) {
            return $firstName = $nameArr[0];
        }
    }

    /**
     * insert ',' into numbers
     */
    public function formatNumber($number)
    {
        if (!$number) return $number;
        return number_format($number);
    }

    public function hexToRgb($hex, $returnAsArray = false)
    {
        try {
            list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
            if ($r && $g && $b) {
                if ($returnAsArray) {
                    return [
                        'R' => $r,
                        'G' => $g,
                        'B' => $b,
                    ];
                } else {
                    return $r . ', ' . $g . ', ' . $b;
                }
            }
        } catch (\Exception $e) {
        }
    }

    public function isInCategory($element, $categoryFieldHandle, $categorySlug)
    {
        if (!empty($element[$categoryFieldHandle])) {
            foreach ($element[$categoryFieldHandle] as $category) {
                if ($category->slug == $categorySlug) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getTelHref($number)
    {
        $temp = str_replace(' ', '-', trim($number));
        $temp = str_replace('(', '', $temp);
        return str_replace(')', '', $temp);
    }

    /**
     * Hmm, maybe the cache thing is useless?
     *
     * Check if a plugin has been loaded and enabled, and cached the result in this class. But it only caches it if it is already loaded and enabled, otherwise, it will check again next time when called.
     * @param  [type]  $pluginHandle [description]
     * @return boolean               true / false
     */
    public function isPluginLoaded($pluginHandle)
    {
        if (!empty($this->cachedCheckedPlugins[$pluginHandle])) {
            return true;
        } else {
            $plugin = Craft::$app->plugins->getPlugin($pluginHandle);
            if ($plugin) {
                $this->cachedCheckedPlugins[$pluginHandle] = true;
                return true;
            }
        }

        return false;
    }

    /**
     * SEOmatic data helper
     * get the social media handle (username), given the name of social media. Social settings are set up in SEOmatic
     * @param  [string] $socialMediaName: Strictly restricted to the following strings (case sensitive):
     * 'facebook', 'twitter', 'linkedIn', 'googlePlus', 'youtube', 'youtubeChannel', 'instagram', 'pinterest', 'github', 'vimeo'
     * @return [string/null]
     */
    public function getSocialMediaHandle($socialMediaName)
    {
        if ($this->isPluginLoaded('seomatic')) {
            $socialSettings = Craft::$app->seomatic->getSocial(Craft::$app->language);
            if (!empty($socialSettings[$socialMediaName . 'Handle'])) {
                return $socialSettings[$socialMediaName . 'Handle'];
            }
        }
    }

    /**
     * SEOmatic data helper
     * get one specific attribute set up in SEOmatic's Site Identity section
     * @param  [string] $attr [description]
     * @return [mixed/null]       [description]
     */
    public function getSiteIdentityAttribute($attr)
    {
        if ($this->isPluginLoaded('seomatic')) {
            $identity = Craft::$app->seomatic->getIdentity(Craft::$app->language);
            if (!empty($identity[$attr])) {
                return $identity[$attr];
            }
        }
    }

    public function highlight($c, $q)
    {
        $q = str_replace(array('', '\\', '+', '*', '?', '[', '^', ']', '$', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '#', '-', '_'), '', $q);
        $c = preg_replace("/($q)(?![^<]*>)/i", "<span class=\"highlight\">\${1}</span>", $c);
        // $q=explode(' ',str_replace(array('','\\','+','*','?','[','^',']','$','(',')','{','}','=','!','<','>','|',':','#','-','_'),'',$q));
        // for($i=0;$i<sizeOf($q);$i++)
        //     $c=preg_replace("/($q[$i])(?![^<]*>)/i","<span class=\"highlight\">\${1}</span>",$c);
        return $c;
    }

    public function searchExcerpt($text, $phrase, $radius = 150, $ending = "...")
    {

        //$text =  preg_replace("/<img[^>]+\>/i", "", $text);
        $text = strip_tags($text);

        $phraseLen = strlen($phrase);

        //don't let the radius be less than the phrase that searched for
        if ($radius < $phraseLen) {
            $radius = $phraseLen;
        }
        $phrases = explode(' ', $phrase);

        //search for instances
        foreach ($phrases as $phraseUnit) {
            $pos = strpos(strtolower($text), strtolower($phraseUnit));
            if ($pos > -1) {
                break;
            } else {
                return false;
            }
        }

        $startPos = 0;
        if ($pos > $radius) {
            $startPos = $pos - $radius;
        }

        $textLen = strlen($text);

        $endPos = $pos + $phraseLen + $radius;
        if ($endPos >= $textLen) {
            $endPos = $textLen;
        }

        $excerpt = substr($text, $startPos, $endPos - $startPos);
        if ($startPos != 0) {
            $excerpt = substr_replace($excerpt, $ending, 0, $phraseLen);
        }

        if ($endPos != $textLen) {
            $excerpt = substr_replace($excerpt, $ending, -$phraseLen);
        }

        //highlight
        $excerpt = $this->highlight($excerpt, $phrase);

        return $excerpt;
    }

    /**
     * Look for query in data recursively.
     * @param  [type] $query          [description]
     * @param  [type] $object         [description]
     * @param array $fieldsToSearch basically most of the plain text, redactor type fields. Reminder to check the fields inside matrix fields as well, because they are not immediately visible in the field list in Craft
     * @return [type]                 [description]
     */
    public function deepBodySearch($query, $object, $fieldsToSearch = array('richText', 'summary', 'subHeading'))
    {

// echo "<pre>";
// echo get_class($object) . "\n";
// echo "</pre>";

        // $elementType = @$object->getElementType();
        // if($elementType) {
        if ($object instanceof \craft\elements\Entry) {
            $block = @$object['body'];
        } else {
            $block = $object;
        }
        if ($block) {
            // print_r($block);
            if ($block instanceof \benf\neo\elements\Block or $block instanceof \craft\elements\MatrixBlock) {
                foreach ($fieldsToSearch as $field) {
                    if (!empty($block[$field])) {
                        $found = $this->searchExcerpt($block[$field], $query);
                        if ($found) {
                            return $found;
                        }
                    }
                }
            }

            // object of the neo field itself
            if ($block instanceof \benf\neo\elements\db\BlockQuery) {
                foreach ($block as $childBlock) {
                    $found = $this->deepBodySearch($query, $childBlock, $fieldsToSearch);
                    if ($found) {
                        return $found;
                    }
                }
            }

            if ($block instanceof \benf\neo\elements\Block and !empty($block['reusableSnippetsArticle'])) {
                $found = $this->deepBodySearch($query, $block['reusableSnippetsArticle'], $fieldsToSearch);
                if ($found) {
                    return $found;
                }
            }

            if ($block instanceof \benf\neo\elements\Block and !empty($block['reusableSnippetsFullWidth'])) {
                $found = $this->deepBodySearch($query, $block['reusableSnippetsFullWidth'], $fieldsToSearch);
                if ($found) {
                    return $found;
                }
            }

            if ($block instanceof \craft\elements\db\ElementQuery) {
                // reusable snippets
                if ($block->elementType == 'craft\elements\Entry') {
                    foreach ($block as $snippet) {
                        $found = $this->deepBodySearch($query, $snippet, $fieldsToSearch);
                        if ($found) {
                            return $found;
                        }
                    }
                }
                // elseif($block->elementType == 'benf\neo\elements\Block') {

                // }
            }

            // if($block instanceof \verbb\supertable\elements\SuperTableBlockElement) {
            //  print_r($block);
            // }

            /** This compactContentMatrix is used in Healthecare, it's unlikely to be used in future projects. However, you can replace this with any matrix field put in Neo if you want the search excerpt to work on the content of this matrix field as well. In the future at some point, maybe we can loop through the fields of a Neo_BlockModel and handle each type separately. */
            // if($block instanceof \benf\neo\elements\Block and !empty($block['compactContentMatrix'])) {
            //  $found = $this->deepBodySearch($query, $block['compactContentMatrix'], $fieldsToSearch);
            //  if($found) {
            //      return $found;
            //  }
            // }

            // if($block instanceof \craft\elements\db\ElementQuery and $block->getElementType()->getClassHandle() == 'MatrixBlock') {
            //  foreach ($block as $childMatrixBlock) {
            //      if($childMatrixBlock instanceof MatrixBlockModel) {
            //          $found = $this->deepBodySearch($query, $childMatrixBlock, $fieldsToSearch);
            //          if($found) {
            //              return $found;
            //          }
            //      }
            //  }
            // }
        }
        // }

        return false;
    }

    public function getLastWordInString($string)
    {
        $pieces = explode(' ', $string);
        return array_pop($pieces);
    }

//  public function getVideoId($videoUrl) {
//      $path = $_SERVER['DOCUMENT_ROOT'] . "/craft/plugins/videoembedutility/twigextensions/VideoEmbedUtilityTwigExtension.php";
//      if(file_exists($path)) {
//          try{
//              require_once($path);
//              $helper = new VideoEmbedUtilityTwigExtension();
//              return $helper->videoId(trim($videoUrl));
//          } catch (\Exception $e) {}
//      }
//  }

    /**
     * Check if a hash string is valid in URL
     * @param  [string] $hash
     * @return [trimmed URL | false if invalid]
     */
    public function validateUrlHash($hash)
    {
        $hash = trim($hash);
        $testUrl = 'http://superdummytest.com#' . $hash;
        if (filter_var($testUrl, FILTER_VALIDATE_URL) === false) {
            return false;
        }

        return $hash;
    }

    /**
     * Using Yump Thumbnailer instead of other thumbnailer like imager
     * @param  [type] $url    [description]
     * @param  [type] $config [description]
     * @return [type]         [description]
     */
    public function getYumpThumbnailerUrl($url, $config)
    {
        $url = str_replace('https://', '/https/', $url);
        $url = str_replace('http://', '/http/', $url);
        $url = str_replace('//', '/', $url);
        $url = trim($url);

        $mode = !empty($config['mode']) ? $config['mode'] : 'crop';
        $width = !empty($config['width']) ? $config['width'] : 9999;
        $height = !empty($config['height']) ? $config['height'] : 9999;

        return "/thumb/" . $width . "x" . $height . "/" . $mode . $url;
    }

    /**
     * Helper function to get a thumbnail of an image asset. Powered by imager
     *
     * Rules:
     * 1. for svg files, return the URL right away, because the transformation might alter the image in an unexpected way.
     * 2. if imager failed for any reasons, we use the Yump thumbnailer as fallback. Refer to getYumpThumbnailerUrl() function above.
     *
     * @param \craft\elements\Asset $image [description]
     * @param array $transform transform settings. re
     * @param boolean $returnImagerObject whether we should return the \aelvan\imager\models\CraftTransformedImageModel object instead - only works for imager. By default, we will return the URL only.
     * @param boolean $useYumpThumbnailerByDefault by default, yump thumbnailer is only used as fallback. But you can set this to true if you want to use it directly. Or, you can use the getYumpThumbnailerUrl() function above directly.
     * @return [type]                      [description]
     */
    public function getThumbnail($image, $transform, $returnImagerObject = false, $useYumpThumbnailerByDefault = false)
    {
        if (empty($image) or !($image instanceof \craft\elements\Asset) or $image->kind != 'image') {
            return null;
        }

        /** just return the URL if it's SVG */
        if (stripos($image->getMimeType(), 'svg') !== FALSE) {
            return $image->getUrl();
        }

        if (!$useYumpThumbnailerByDefault) {
            try {
                // use the focal point if not applied yet
                if (empty($transform['position'])) {
                    $transform['position'] = $image->getFocalPoint();
                }

                // run imagerX
                $thumbnailObject = ImagerX::$plugin->imager->transformImage($image, $transform, null, null);

                if ($thumbnailObject) {
                    if ($returnImagerObject) {
                        return $thumbnailObject;
                    } else {
                        return $thumbnailObject->url;
                    }
                } else {
                    throw new \Exception("Got empty thumbnail by running imagerX->transformImage for unknown reasons.");
                }
            } catch (\Exception $e) {
                Craft::error("Failed to get thumbnail using imagerX. Error message: " . $e->getMessage(), __METHOD__);
            }
        }

        // use Yump Thumbnailer as fallback.
        return $this->getYumpThumbnailerUrl($image->getUrl(), $transform);
    }

    public function isNavItemActive($element)
    {
        if (!empty($element)) {
            $segments = Craft::$app->request->getSegments();
            $slug = $element->slug;
            if ($slug and in_array($slug, $segments)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Craft::$app->sections->getEntryTypesByHandle returns an array instead of just one. My guess is there could be entry types in different sections using the same handle. So, to get a unique entry type, we pass in a second param '$sectionHandle' to pinpoint one entry type model
     * @param  [type] $handle        [entry type handle]
     * @param  [type] $sectionHandle [section handle]
     * @return [EntryTypeModel|null]                [description]
     */
//  public function getEntryTypeByHandle($handle, $sectionHandle) {
//      $entryTypes = Craft::$app->sections->getEntryTypesByHandle($handle);
//      if(!empty($entryTypes)) {
//          foreach ($entryTypes as $entryType) {
//              $section = $entryType->getSection();
//              if($section and $section->handle == $sectionHandle) {
//                  return $entryType;
//              }
//          }
//      }
//  }
//
//  public function getCategoriesByGroupHandle($categoryGroupHandle, $returnTitlesOnly = false) {
//      $criteria = Craft::$app->elements->getCriteria(ElementType::Category);
//      $criteria->group = $categoryGroupHandle;
//      $criteria->limit = null;
//      $categories = $criteria->find();
//      if(!$returnTitlesOnly) {
//          return $categories;
//      } else {
//          $categoryTitles = [];
//          foreach ($categories as $category) {
//              $categoryTitles[] = $category->title;
//          }
//          return $categoryTitles;
//      }
//  }

    /**
     * Extract (from Mailchimp form embedded code) the form action URL string and construct the bot hidden field value string based on that, so the admin only need to copy & paste in the entire embedded code without manually extract the two strings into CMS.
     * @return array|null if fail.
     * Example: array('action' => 'https://yump.us3.list-manage.com/subscribe/post?u=e4429d6397aaf2320b515bb93&amp;id=d68e372e21', 'botId' => 'b_e4429d6397aaf2320b515bb93_d68e372e21')
     */
    public function extractMailchimpFormData($embeddedCode)
    {
        $matches = array();
        preg_match('/<form action="(((?!").)+)"/', $embeddedCode, $matches);

        if (!empty($matches[1]) and strpos($matches[1], '"') === false) {
            $action = $matches[1];
            $m = [];
            preg_match('/u=([a-zA-Z0-9]+)&amp;id=([a-zA-Z0-9]+)/', $action, $m);

            if (!empty($m[1]) and !empty($m[2])) {
                $botId = 'b_' . $m[1] . '_' . $m[2];
                return [
                    'action' => $action,
                    'botId' => $botId,
                ];
            }
        }
    }

    /**
     * Craft put this 'p' param in the URL, we probably want to kill that to make the URL cleaner.
     * @return string: constructed URL params. E.g. query=test&location=vic
     */
    public function getUrlParams()
    {
        $paramString = '';
        $usableParams = [];
        foreach ($_GET as $key => $value) {
            if ($key != 'p') {
                $usableParams[] = $key . '=' . $value;
            }
        }
        $paramString = implode('&', $usableParams);

        return $paramString;
    }

    /**
     * The craft's craft.request.getSegments only works for current URL. But this function use URL as a param, so you can get segments of any URL.
     * @param  [string] $url [description]
     * @return [type]      [description]
     */
    public function getUrlSegments($url)
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        return explode('/', $uri_path);
    }

    /**
     * Get excerpt from string
     *
     * @param String $str String to get an excerpt from
     * @param Integer $startPos Position int string to start excerpt from
     * @param Integer $maxLength Maximum length the excerpt may be
     * @return String excerpt
     */
    public function getExcerpt($str, $startPos = 0, $maxLength = 100)
    {
        if (strlen($str) > $maxLength) {
            $excerpt = substr($str, $startPos, $maxLength - 3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt = substr($excerpt, 0, $lastSpace);
            $excerpt .= '...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function ljDynamicFieldToArray($fieldVal)
    {
        if (!empty($fieldVal)) {
            // so weird, in Live Preview, this value is automatically an array for some reasons
            if (is_array($fieldVal)) {
                return $fieldVal;
            }

            $arr = json_decode($fieldVal, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                // ljDynamicField returns a single item (not array) if there's only one item is selected
                if (!is_array($arr)) {
                    $arr = [$arr];
                }

                return $arr;
            }
        }
        return null;
    }

    public function checkScriptPermission($allowAdmins = false, $allowUserGroups = array())
    {
        $PASSWORD = Craft::$app->config->custom->controllerActionsPassword;
        $ALLOWED_IPs = Craft::$app->config->custom->controllerActionsAllowedIps;

        // TODO: support $allowUserGroups as well

        return
            // is admin
            ($allowAdmins && Craft::$app->getUser() && Craft::$app->getUser()->isAdmin)
            or
            // password correct and IP is whitelisted
            (
                !empty($PASSWORD) &&
                !empty($ALLOWED_IPs) &&
                is_array($ALLOWED_IPs) &&
                Craft::$app->getRequest()->getParam('a') == $PASSWORD &&
                (in_array($_SERVER['REMOTE_ADDR'], $ALLOWED_IPs)
                    || (!empty($_SERVER['HTTP_CF_CONNECTING_IP']) && in_array($_SERVER['HTTP_CF_CONNECTING_IP'], $ALLOWED_IPs)))
            ); // sometimes REMOTE_ADDR shows Cloudflare IPs if going through Cloudflare, but Cloudflare has another attribute telling us what the original IP is.
    }

    /**
     * Craft3's config service doesn't have a 'get' function which you can retrieve an attribute without exceptions thrown. Now in Craft 3, if attribute is not found in config file, it will throw a UnknownPropertyException error. So we wrote our custom method to 'safely' get the config attributes.
     * @param  [type] $attrName [description]
     * @param string $category [e.g. general, db, yump, etc]
     * @return [mixed|null]     return null if not found
     */
    public function getConfig($attrName, $category = 'custom')
    {
        try {
            return Craft::$app->config->$category->$attrName;
        } catch (\Exception $e) {
        }
        // } catch (\yii\base\UnknownPropertyException $e) {}
    }

    /**
     * Get a param which is passed to a route / template.
     * @param  [type] $paramName [description]
     * @return [type]            [description]
     */
    public function getRouteParam($paramName)
    {
        $params = Craft::$app->getUrlManager()->getRouteParams();
        return @$params[$paramName];
    }

    /**
     * Pretty dump a param and kill the process (if set the $terminate = true)
     * @param  [mixed] $param [description]
     * @return [type]        [description]
     */
    public function dump($param, $terminate = true)
    {
        echo "<pre>";
        print_r($param);
        echo "</pre>";

        if ($terminate) {
            exit();
        }
    }

    public function log($message, $logFileName = 'yump', $category = 'info')
    {
        $file = Craft::getAlias('@storage/logs/' . $logFileName . '.log');

        if (is_array($message)) {
            $message = print_r($message, TRUE);
        }

        $log = date('Y-m-d H:i:s') . ' [' . strtoupper($category) . ']' . $message . "\n";

        \craft\helpers\FileHelper::writeToFile($file, $log, ['append' => true]);
    }

    /**
     * To render a template within a plugin. Sometimes it's hard to use include on twig, because the current template path might have changed already.
     *
     * HINT: you might want to apply the | raw filter on twig
     * @param  [type] $pluginHandle [description]
     * @param  [type] $path         [description]
     * @param array $data [description]
     * @return [type]               [description]
     */
    public function displayPluginTemplate($pluginHandle, $path, $data = array())
    {
        try {
            $oldMode = Craft::$app->view->getTemplateMode();
            Craft::$app->view->setTemplateMode(\craft\web\View::TEMPLATE_MODE_CP);
            $html = Craft::$app->view->renderTemplate($pluginHandle . '/' . trim($path, '/'), $data);
            Craft::$app->view->setTemplateMode($oldMode);
            return $html;
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * In Craft 3, in some cases, it's impossible to just do $_POST[$paramName] = $value to set a request body param, because Craft might have getValidatedBodyParam() function to get a body param securely. If you look into that function, you will see Craft::$app->getSecurity()->validateData($value), which requires the data to be hashed.
     *
     * Now, it's NOT just a matter of doing $_POST[$paramName] = Craft::$app->getSecurity()->hashData($value), because if you look into the getBodyParam() function inside getValidatedBodyParam(), it gets the param value from getBodyParams(), which is likely to return a already cached _bodyParams array that has already been set (cached) in previous code.
     *
     * So, the proper way to override this request param in PHP is to get all the bodyParams, update that particular param we want in the array, and call setBodyParams to update the whole _bodyParams array again.
     * @param [string] $paramName [description]
     * @param [mixed] $value     [description]
     */
    public function setBodyParam($paramName, $value)
    {
        $params = Craft::$app->request->getBodyParams();
        $params[$paramName] = Craft::$app->getSecurity()->hashData($value);
        Craft::$app->request->setBodyParams($params);
    }

    public function extractOnlyNumbers($str)
    {
        return (int)filter_var($str, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Get all the data needed for rendering a banner.
     *
     * NOTE: if the project needs additional fields / settings, you should copy and paste this function into the project's module service, and modify the function to suit your own needs.
     *
     * @param craft\elements\Entry $entry the entry which hosts the banner
     * @return array: the banner attributes
     * Example return data:
     * [
     * 'heading' => string: 'This is the heading',
     * 'summary' => null, or string: 'This is the summary',
     * 'buttons' => null, or array(
     * typedlinkfield\models\Link,
     * // typedlinkfield\models\Link, // up to two buttons
     * ),
     * 'media' => null, or array(
     * 'type' => 'image' or 'video',
     * 'content' => craft\elements\Asset (limited to be image type), or the video field value (plain text field, with video URL),
     * // if media has special settings (currently only used by video)
     * 'settings' => [
     * 'transcriptEntry' => craft\elements\Entry (Video Transcript entry)
     * 'videoHtmlTitle' => string,
     * ],
     * ),
     * ]
     */
    public function getBanner($entry, $variation = '')
    {
        if (!empty($entry['banner'])) {
            $banner = $entry->banner;
            $featuredEntryInBanner = $entry->featuredEntryInBanner && count($entry->featuredEntryInBanner->all()) > 0 ? $entry->featuredEntryInBanner : null;

            // get textContent block and media block
            $textContentBlock = null;
            $mediaBlock = null;
            foreach ($banner->level(1)->all() as $block) {
                $handle = $block->getType()->handle;
                if ($handle == 'textContent') {
                    $textContentBlock = $block;
                } else if ($handle == 'media') {
                    $mediaBlock = $block;
                }
            }

            // if no even textContent, the banner will be considered empty, even if media block is present.
            if ($textContentBlock) {
                // getting data of textContent
                $heading = $textContentBlock->heading;
                // if heading not specified, use the entry's heading
                if (empty(trim($heading))) {
                    $heading = $entry->title;
                }
                $summary = null;
                $buttons = null;
                foreach ($textContentBlock->getChildren()->all() as $block) {
                    $handle = $block->getType()->handle;
                    if ($handle == 'summary') {
                        $summary = $block->summary;
                        // if summary not specified, use entry's summary
                        if (empty($summary)) {
                            $summary = !empty($entry['summary']) ? $entry->summary : null;
                        }
                    } else if ($handle == 'buttons') {
                        foreach ($block->getChildren()->all() as $ctaBlock) {
                            if ($ctaBlock->getType()->handle == 'callToAction') {
                                if ($buttons === null) {
                                    $buttons = [];
                                }

                                // the cta field should have been set as required in CMS, so we assume it will never be empty.
                                // We only return typedlinkfield\models\Link because the front end can use cta macro the render the button
                                $buttons[] = $ctaBlock->cta;
                            }
                        }
                    }
                }

                // getting data of media
                $media = null;
                if ($mediaBlock) {
                    foreach ($mediaBlock->getChildren()->all() as $block) {
                        $handle = $block->getType()->handle;
                        if ($handle == 'image') {
                            foreach ($block->getChildren()->all() as $childBlock) {
                                $childBlockHandle = $childBlock->getType()->handle;
                                $imageSource = null;
                                if ($childBlockHandle == 'useFeaturedImage') {
                                    if (!empty($entry['featuredImage'])) {
                                        $imageSource = $entry->featuredImage;
                                    }
                                } else if ($childBlockHandle == 'customImage') {
                                    if (!empty($childBlock['image'])) {
                                        $imageSource = $childBlock->image;
                                    }
                                }
                                if ($imageSource) {
                                    $image = $imageSource->one();
                                    if ($image) {
                                        $media = [
                                            'type' => 'image',
                                            'content' => $image,
                                        ];
                                    }
                                }
                            }
                        } else if ($handle == 'video') {
                            if (!empty($block['video'])) {
                                $media = [
                                    'type' => 'video',
                                    'content' => $block->video,
                                    'settings' => [
                                        'transcriptEntry' => $block->videoTranscript ? $block->videoTranscript->one() : null,
                                        'videoHtmlTitle' => $block->videoHtmlTitle,
                                    ],
                                ];
                            }
                        }
                    }
                }

                $variationClasses = $media ? 'banner--with-media ' . $variation : 'banner--simple ' . $variation;

                $variationClasses .= $featuredEntryInBanner ? 'banner--with-featured-entry ' : '';

                return [
                    'heading' => $heading,
                    'summary' => $summary,
                    'buttons' => $buttons,
                    'media' => $media,
                    'variationClasses' => $variationClasses,
                    'featuredEntryInBanner' => $featuredEntryInBanner
                ];
            }
        }
    }

    public function entryIsInTopMenu($entry)
    {
        $topMenu = $this->getGlobalFieldValue('menus', 'entries');
        return $this->findItemInNav($topMenu, $entry->id);
    }

    /**
     * Get the breadcrumb items, for a page type entry.
     *
     * @param craft\elements\Entry $entry (assuming an entry in pages section, or can be used in any other structure type sections as well. This function doesn't work for channel type sections)
     * @param boolean $includingHome whether we should include the home crumb or not. According to our designer, home crumb should only be included if Home is not present in the top menu.
     * @return array        array of the crumbs.
     */
    public function getPageEntryCrumbs($entry, $includingHome = true, $includingSelf = true)
    {
        $crumbs = [];

        // include home if required as icon
        if ($includingHome) {
            $crumbs[] = [
                'text' => '',
                'url' => '/',
                'icon' => true
            ];
        }

        // include ancestors
        $ancestors = $entry->getAncestors()->all();
        foreach ($ancestors as $ancestor) {
            // if not in top menu and ->hideFromNavigation('1') is on then continue
            if ($ancestor->hideFromNavigation == '1' && !$this->entryIsInTopMenu($ancestor)) {
                continue;
            }

            $crumb = [
                'text' => $ancestor->title,
                'url' => $ancestor->getUrl(),
                'icon' => false
            ];
            if ($ancestor->getType()->handle == 'linkOnly') {
                $crumb['target'] = $ancestor->cta->getTarget();
            }
            $crumbs[] = $crumb;
        }

        if ($includingSelf) {
            // flag the parent
            if (!empty($crumbs)) {
                $crumbs[count($crumbs) - 1]['isParent'] = true;
            }

            // include current entry at the end.
            $crumbs[] = [
                'text' => $entry->title,
            ];
        }

        return $crumbs;
    }

    public function isElementQuery($object)
    {
        return $object instanceof \craft\elements\db\ElementQuery;
    }

    public function isEntryQuery($object)
    {
        return $object instanceof \craft\elements\db\EntryQuery;
    }

    public function isNeoBlockQuery($object)
    {
        return $object instanceof \benf\neo\elements\db\BlockQuery;
    }

    /**
     * Translate different $card types into the same card meta attributes.
     * @param mixed $card
     * @param string|false $forceCardType if we want to force the card type (usually it's 'image' or 'text-only', auto-detected if this is coming from a neo block). Mainly use this for situations when you need to manually include the cards template and set the cards attr to an ElementQuery (e.g. EntryQuery)
     * @param array $imagerSettings if we need custom imagerSettings. Without it, we still have default settings defined in general config file (general.php)
     * @return array|null if not an Entry, nor a Block
     */
    public function translateCardBio($card, $forceCardType = false, $imagerSettings = array())
    {

        $heading = null;
        $summary = null;
        $image = null;
        $thumbnailUrl = null;
        $imageTitle = null;
        $url = null;
        $secondaryUrl = null;
        $date = null;
        $ctaText = $this->getConfig('defaultCardCtaText');
        $secondaryCtaText = '';
        $ctaTarget = null;
        $cardType = $forceCardType ?: 'image';

        if ($card instanceof \craft\elements\Entry) {
            $heading = $card->title;
            if ($cardType != 'news') {
                $summary = @$card->summary;
            }
            if ($cardType == 'news') {
                $date = @$card->postDate->format('d M Y');
            }
            if ($cardType == 'image') {
                $image = $card->featuredImage->one();
                $imageTitle = $image->title ?? '';
            }
            $url = $card->url;
        } else if ($card instanceof \benf\neo\elements\Block) {
            $neoBlockHandle = $card->getType()->handle;
            if ($neoBlockHandle == 'imageCard') {
                $cardType = $forceCardType ?: 'image';
                $cardMatrix = $card->imageCard;
            } else if ($neoBlockHandle == 'textOnlyCard') {
                $cardType = $forceCardType ?: 'text-only';
                $cardMatrix = $card->textOnlyCard;
            } else if ($neoBlockHandle == 'fullWidthLandscapeCard') {
                $cardType = $forceCardType ?: 'landscape';
                $cardMatrix = $card->imageCard;
            }
            // $this->dump($cardMatrix);
            foreach ($cardMatrix->all() as $childBlock) {
                $childBlockHandle = $childBlock->getType()->handle;
                if ($childBlockHandle == 'chooseFromEntries') {
                    $sourceEntry = $childBlock->entry->one();
                    // re-use this function to get card bio from the entry
                    $cardBio = $this->translateCardBio($sourceEntry, $forceCardType, $imagerSettings);
                    $cardBio['cardType'] = $cardType; // overwrite card type
                    // update the ctaText if customCtaText is defined.
                    $customCtaText = trim($childBlock->customCtaText);
                    if (!empty($customCtaText)) {
                        $cardBio['ctaText'] = $customCtaText;
                    }
                    return $cardBio;
                } else if ($childBlockHandle == 'chooseAReportCategory') {
                    $category = $childBlock->reportCategory->one();
                    $heading = $category->title ?? '';
                    $summary = $category->summary ?? '';
                    if ($cardType == 'image' || $cardType == 'landscape') {
                        $imageField = $category->featuredImage ?? null;
                        $image = $imageField ? $imageField->one() : null;
                    }
                    $entryToLinkToField = $category->singleEntry ?? null;
                    $entryToLinkTo = $entryToLinkToField ? $category->singleEntry->one() : null;
                    if (!empty($entryToLinkTo)) {
                        $url = $this->generateValidUrl($entryToLinkTo->url);
                        $ctaText = 'View all';
                    }
                    if ($childBlock->addLinkToLatestInCategory) {
                        // $entryToLinkTo->extraLinkLabel;
                        $latestEntryInCategory = Entry::find()
                            ->section('reports')
                            ->reportType($category)
                            ->orderBy('postDate desc')
                            ->limit(1)
                            ->one();
                        if (isset($latestEntryInCategory)) {
                            $secondaryCtaText = $childBlock->extraLinkLabel ?? 'View the latest from ' . $heading;
                            $secondaryUrl = $this->generateValidUrl($latestEntryInCategory->url);
                        }
                    }
                } else if ($childBlockHandle == 'manualInput') {
                    $heading = $childBlock->heading ?? '';
                    $summary = $childBlock->summary ?? '';
                    if ($cardType == 'image' or $cardType == 'landscape') {
                        $image = $childBlock->image->one(); // if you get error here, there's something wrong in your CMS settings. Try to fix the settings instead of hack this around.
                        $imageTitle = $image->title ?? '';
                    }
                    $linkField = $childBlock->linkTo;
                    if (!empty($linkField)) {
                        $url = $this->generateValidUrl($linkField->getUrl());
                        $ctaText = $linkField->getCustomText($ctaText);
                        $ctaTarget = $linkField->getTarget();
                    }
                }
            }
        } else {
            return;
        }

        if ($image) {
            $thumbnailUrl = $this->getImageCardThumbnail($image, $imagerSettings);
            $imageTitle = $image->title ?? '';
        }

        return [
            'heading' => $heading,
            'date' => $date,
            'summary' => $summary,
            // 'image' => $image, // we don't really need it, but return it anyway just in case.
            'thumbnailUrl' => $thumbnailUrl,
            'imageTitle' => $imageTitle,
            'url' => $url,
            'secondaryUrl' => $secondaryUrl,
            'secondaryCtaText' => $secondaryCtaText,
            'ctaText' => $ctaText,
            'ctaTarget' => $ctaTarget,
            'cardType' => $cardType,
        ];
    }

    public function getImageCardThumbnail($image, $imagerSettings = array())
    {
        if ($image) {
            $imagerSettings = $imagerSettings ?? $this->getConfig('imageCardImagerDefaultSettings');
            // $imagerSettings = array_merge_recursive($defaultImagerSettings, $imagerSettings);
            return $this->getThumbnail($image, $imagerSettings);
        }
    }

    public function getNav($forMainNav = false)
    {
        $navHandle = $forMainNav ? 'mainNav' : 'siteNav';
        if (empty($this->_cachedNavPhpArray[$navHandle])) {
            // Craft::info("get fresh php array:$navHandle", __METHOD__);

            $includeHome = $this->getConfig('navShouldIncludeHomepage');
            if($forMainNav) {
                $adapter = new YumpMainNavAdapter(
                    ['includeHome' => $includeHome ? true : false]
                );
            } else {
                $adapter = new YumpNavAdapter(
                    ['includeHome' => $includeHome ? true : false] // whether we should include homepage in the nav list. If this doesn't work, you will probably need to clear cache, because it's only used in getFreshContent() function.
                );
            }
            

            $this->_cachedNavPhpArray[$navHandle] = $adapter->getContent();
        }

        // else {
        //     Craft::info("get CACHED php array:$navHandle", __METHOD__);
        // }

        return $this->_cachedNavPhpArray[$navHandle];
    }

    public function forceUpdateNav($forMainNav = false)
    {
        $cacheName = $forMainNav ? "Main Nav" : "Site Nav";
        try {
            $includeHome = $this->getConfig('navShouldIncludeHomepage');
            if($forMainNav) {
                $adapter = new YumpMainNavAdapter(
                    ['includeHome' => $includeHome ? true : false] // whether we should include homepage in the nav list.
                );
            } else {
                $adapter = new YumpNavAdapter(
                    ['includeHome' => $includeHome ? true : false] // whether we should include homepage in the nav list.
                );
            }
            
            if ($adapter->forceUpdateCache()) {
                return array(
                    'success' => true
                );
            } else {
                throw new \Exception("Setting $cacheName cache was not successful. Please refer to yump.log for details.");
            }
        } catch (\Exception $e) {
            return array(
                "error" => "Failed to update $cacheName cache. Error message: " . $e->getMessage(),
            );
        }
    }

    public function findItemInNav($navItems, $entryId)
    {
        foreach ($navItems->all() as $navItem) {
            if ($navItem['id'] == $entryId) {
                return $navItem;
            } else {
                if ($navItem['children']) {
                    $maybeFoundItem = $this->findItemInNav($navItem['children'], $entryId);
                    if ($maybeFoundItem) {
                        return $maybeFoundItem;
                    }
                }
            }
        }

        return false;
    }

    public function findParentItemInNav($navItems, $entryId, $entry = null)
    {

        if (isset($entry) && $entry->type->handle == 'reports') {
            // Get report type:
            $reportTypeEntry = $entry->reportType[0];
            // Get Landing page field of report type:
            $reportTypeLandingPage = $reportTypeEntry->singleEntry[0];
            return $reportTypeLandingPage;
        }

        foreach ($navItems as $navItem) {
            if (in_array($entryId, $navItem['childrenIds'])) {
                return $navItem;
            } else {
                if ($navItem['children']) {
                    $maybeFoundItem = $this->findParentItemInNav($navItem['children'], $entryId, $entry);
                    if ($maybeFoundItem) {
                        return $maybeFoundItem;
                    }
                }
            }
        }

        return false;
    }

    public function yump_json_decode($content, $exceptionMessage = null, $compressException = false)
    {
        $contentInArray = json_decode($content, true);
        $jsonLastError = json_last_error();
        if ($jsonLastError === JSON_ERROR_NONE) {
            return $contentInArray;
        } else {
            if (!$compressException) {
                $exceptionMessage = $exceptionMessage ?? "Failed to json_decode the data";
                throw new \Exception($exceptionMessage . ' Json decode error was: ' . $jsonLastError . ".");
            }
            Craft::error($exceptionMessage . ' Json decode error was: ' . $jsonLastError . ".", __METHOD__);
        }
    }

    /**
     * It is possible that when you do $entry->id, or entry.id in Twig, you get the draft's element ID, which might not be the entry id we are looking for. This function is to get draft's sourceId (which is the active entry's ID) if it detects it is a draft
     * @param  Entry $entry [description]
     * @return int|null [description]
     */
    public function getEntryId(Entry $entry): ?int
    {
        if ($entry->getIsDraft()) {
            return $entry->getCanonicalId();
        }
        return $entry->id;
    }

    /**
     *
     * @param  [array - entries] $featuredPosts [entries selected by the admin to feature]
     * @param  [string] $contentFeed [matches the entryType handle]
     * @return [array] [array of featured posts and main set of posts]
     */
    public function getLandingPageEntries($featuredPosts, $contentFeed)
    {
        $posts = [];
        $featuredPostIDs = [];

        $numPosts = sizeof($featuredPosts);
        if ($numPosts < 2) {
            //If less than 2, get the latest posts to supplement the featured set of articles with the latest entries
            $exclude = '';

            // $criteria = Entry::find()->section($contentFeed)->orderBy('postDate desc');
            // if($numPosts == 1) {
            //     $exclude = $featuredPosts[0]['id'];
            //     $criteria->id(['not', $exclude])
            // }

            //Exclude the one that the admin has already specified (if they have specified one)
            if ($numPosts == 1) {
                $exclude = $featuredPosts[0]['id'];
                $supplementaryPosts = Entry::find()
                    ->section($contentFeed)
                    ->orderBy('postDate desc')
                    ->limit(2 - $numPosts)
                    ->id(['not', $exclude])
                    ->all();
            } else {
                $supplementaryPosts = Entry::find()
                    ->section($contentFeed)
                    ->orderBy('postDate desc')
                    ->limit(2 - $numPosts)
                    ->all();
            }
            $posts['featured'] = array_merge($featuredPosts, $supplementaryPosts);
        } else {
            //admin has set two themselves, use those for the featured posts
            $posts['featured'] = $featuredPosts;
        }

        //Get ID's of the featured posts so we can use those ID's to exclude them from the main post list
        foreach ($posts['featured'] as $featured) {
            $featuredPostIDs[] = $featured['id'];
        }

        $posts['main'] = Entry::find()
            ->section($contentFeed)
            ->orderBy('postDate desc')
            ->id('and, not ' . implode(', not ', $featuredPostIDs))
            ->limit(6);

        return $posts;

    }

    /**
     * Get all entry types of the website. Has the ability to return the data as array
     * @param boolean $toArray true: return each entry type as array. false: return each as EntryType object
     * @param boolean $handleAsKey only works if $toArray is true. Determine whether id or handle is used as the array key
     * @return [type]               [description]
     */
    public function getAllEntryTypes($toArray = false, $handleAsKey = false)
    {
        $siteEntryTypes = Craft::$app->sections->getAllEntryTypes();

        if ($toArray) {
            $result = [];

            foreach ($siteEntryTypes as $key => $entryType) {
                $obj = [
                    'id' => $entryType->id,
                    'name' => $entryType->name,
                    'sectionId' => $entryType->sectionId,
                    'handle' => $entryType->handle,
                    'fieldLayoutId' => $entryType->fieldLayoutId,
                    'sortOrder' => $entryType->sortOrder,
                    'hasTitleField' => $entryType->hasTitleField,
                    'titleFormat' => $entryType->titleFormat,
                    'uid' => $entryType->uid,
                ];

                if ($handleAsKey) {
                    $result[$obj['handle']] = $obj;
                } else {
                    $result[$obj['id']] = $obj;
                }
            }
            return $result;
        } else {
            return $siteEntryTypes;
        }
    }

    /**
     *
     * @param  [array] $filters Search filters are passed in this format: ['pages' => 1, 'resources' => 0, 'news' => 1]. We want to convert it to the format we are gonna use in searchNew().
     * @return [array]          e.g.
     * [
     *     'entries' => ['1125', '3533', // ... entry type IDs],
     *     'assets' => [''],
     * ]
     */
    // private function _prepareSearchFilters($filters) {
    //     $entryTypeIds = [];
    //     $assetVolumeIds = [];

    //     // $siteEntryTypes = $this->getAllEntryTypes(true, true);

    //     $pagesSection = Craft::$app->sections->getSectionByHandle('pages');
    //     $pagesEntryTypes = $pagesSection->getEntryTypes();
    //     foreach ($pagesEntryTypes as $entryType) {
    //         if(!empty($filters['pages'])) { // if including 'pages'
    //             // if(empty($filters['schools'])) {
    //             //     if($entryType->handle == 'school') {
    //             //         continue; // skip school type
    //             //     }
    //             // }
    //             $entryTypeIds[] = $entryType->id;
    //         } else { // if not including normal 'pages'
    //             // only include resource type
    //             // if(!empty($filters['schools']) and $entryType->handle == 'school') {
    //             //     $entryTypeIds[] = $entryType->id;
    //             // }
    //         }
    //     }

    //     foreach ($filters as $filterName => $filterBool) {
    //         // assume only 'documents' is the only assets, all the others are entries
    //         if($filterName == "documents" && $filterBool) {
    //             $volume = Craft::$app->volumes->getVolumeByHandle($filterName);
    //             if($volume) {
    //                 $assetVolumeIds[] = $volume->id;
    //             }
    //         } else {
    //             if($filterBool) {
    //                 if($filterName != 'pages' 
    //                     // and $filterKey != 'pages'
    //                 ) {
    //                     $section = Craft::$app->sections->getSectionByHandle($filterName);
    //                     if($section) {
    //                         $entryTypes = $section->getEntryTypes();
    //                         foreach ($entryTypes as $entryType) {
    //                             $entryTypeIds[] = $entryType->id;
    //                         }
    //                     }
    //                 }

    //                 // if(!empty($siteEntryTypes[$filterName])) {
    //                 //     $entryTypeIds[] = $siteEntryTypes[$filterName]['id'];
    //                 // }
    //             }
    //         }
    //     }

    //     $result = [];
    //     if(!empty($assetVolumeIds)) {
    //         $result['assetVolumes'] = $assetVolumeIds;
    //     }
    //     if(!empty($entryTypeIds)) {
    //         $result['entryTypes'] = $entryTypeIds;
    //     }

    //     return $result;
    // }

    /**
     * Get the list of types from 'searchFiltersMapping' in general.php
     * @param  [type] $type 'entryType', or 'assetVolume'
     * @return [array] array of ids
     */
    private function _getIdsForSearch($type)
    {
        $searchFiltersMapping = $this->getConfig('searchFiltersMapping');

        $ids = [];
        if ($searchFiltersMapping) {
            foreach ($searchFiltersMapping as $key => $settings) {
                if ($settings['type'] == $type) {
                    $ids = array_merge($ids, $settings['ids']);
                }
            }
        }

        return $ids;
    }

    /**
     * Get the breadcrumb of an entry
     * @param  [type] $entry [description]
     * @return [array]        the breadcrumb structure
     */
    public function getBreadcrumb($entry, $addSelf = true, $linkToSelf = false, $excludeHome = false)
    {

        $crumbs = [];

        // Shall we include Home crumb? if we don't have homepage in the nav list, then the breadcrumb should include Home crumb; otherwise it shouldn't.
        $breadcrumbShouldIncludeHome = $excludeHome ? false : $this->getConfig('breadcrumbShouldIncludeHome');
        if ($breadcrumbShouldIncludeHome) {
            $crumbs[] = [
                'url' => '/',
                'icon' => true,
            ];
        }

        $generalSettings = \Craft::$app->getGlobals()->getSetByHandle('generalSettings');
        $sectionHandle = $entry->getSection()->handle;

        // get crumbs based on the section
        $nav = $this->getNav();
        switch ($sectionHandle) {

            // if this is under 'pages' entry, simply get the path from cached nav
            case 'pages':
                $crumbs = $this->buildFamilyCrumbs($crumbs, $nav, $entry['id']);
                $crumbs[array_key_last($crumbs)]['isParent'] = true;
                break;

            // if this is in a separate section (e.g. news, as a channel). Its parent is a landing page under 'pages' - so get the breadcrumb of the landing page first. Reminder to set the landing page ID in general.php
            case 'reports':
                $reportTypeLandingPage = \modules\ewovmodule\EWOVModule::$instance->ewov->findReportLandingPageByEntry($entry);
                if($reportTypeLandingPage) {
                    // normally we set the parent of a report page to the landing page of its type
                    $crumbs = $this->buildFamilyCrumbs($crumbs, $nav, $reportTypeLandingPage['id']);
                    $crumbs[] = [
                            'text' => $reportTypeLandingPage['title'],
                            'url' => $reportTypeLandingPage['url'],
                            'isParent' => true,
                        ];
                } else {
                    // if the report type landing page is not found, we fall back to the data and reports landing page as the parent
                    $dataAndReportsLandingPage = $generalSettings->dataAndReportsLandingPage->one();
                    if($dataAndReportsLandingPage) {
                        $crumbs = $this->buildFamilyCrumbs($crumbs, $nav, $dataAndReportsLandingPage['id']);
                        $crumbs[] = [
                            'text' => $dataAndReportsLandingPage['title'],
                            'url' => $dataAndReportsLandingPage['url'],
                            'isParent' => true,
                        ];
                    }
                }
                
                break;

            case 'news':
                $newsLandingPage = $generalSettings->newsLandingEntry->one();
                if($newsLandingPage) {
                    $crumbs = $this->buildFamilyCrumbs($crumbs, $nav, $newsLandingPage['id']);
                    $crumbs[] = [
                        'text' => $newsLandingPage['title'],
                        'url' => $newsLandingPage['url'],
                        'isParent' => true,
                    ];
                }
                break;

            case 'stories':
                return [];

            // ... add more sections similar to 'news' here
            
            default:
                // do nothing, so "Home" will be the only crumb before self
                break;
        }

        // shall we add self?
        if($addSelf) {
            $crumbs[] = [
                'text' => $entry['title'],
                'url' => $linkToSelf ? $entry['url'] : false
            ];
        }

        return $crumbs;
    }

    public function buildFamilyCrumbs($crumbs, $branch, $entryId)
    {
        foreach ($branch as $page) {
            if (!empty($page['descendantsIds']) and in_array($entryId, $page['descendantsIds'])) {

                // if the target page is a descendant, add the current page to the crumbs anyway.
                $crumbs[] = [
                    'text' => $page['title'],
                    'url' => $page['url'],
                ];

                // if the target page is not the children, then do the same search in the children recursively
                if (!in_array($entryId, $page['childrenIds']) and !empty($page['children'])) {
                    $crumbs = $this->buildFamilyCrumbs($crumbs, $page['children'], $entryId);
                } else {
                    $crumbs[array_key_last($crumbs)]['isParent'] = true; // set the last item in crumb to be the parent. This attribute is useful for mobile
                }
            }
        }

        return $crumbs;
    }

    private function _getFilterKey($element)
    {
        $searchFiltersMapping = $this->getConfig('searchFiltersMapping');
        foreach ($searchFiltersMapping as $key => $settings) {
            if ($element instanceof Entry) {
                if ($settings['type'] == 'entryType' and in_array($element['typeId'], $settings['ids'])) {
                    return $key;
                }
            } else if ($element instanceof Asset) {
                if ($settings['type'] == 'assetVolume' and in_array($element['volumeId'], $settings['ids'])) {
                    return $key;
                }
            }
        }
    }

    private function _buildEntryDataForSearch($entry, $query)
    {
        $result = [
            'id' => $entry['id'],
            'score' => @$entry['searchScore'],
            'title' => $entry['title'],
            'url' => $entry['url'],
            'type' => 'entryType',
            'typeId' => $entry['typeId'],
            'filterKey' => $this->_getFilterKey($entry),
            'summary' => null,
            'tags' => array(),
            'breadcrumb' => $this->getBreadcrumb($entry, true, true, true),
            'highlightedTitle' => $this->searchExcerpt($entry['title'], $query),
        ];


        // get tags
        $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');
        if ($searchTagFieldHandle && !empty($entry[$searchTagFieldHandle])) {
            foreach ($entry[$searchTagFieldHandle] as $tag) {
                $result['tags'][] = $tag['title'];
            }
        }


        // get summary
        $summaryObj = @$entry['summary'];
        if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
            $result['summary'] = $summaryObj->__toString();
            $result['highlightedSummary'] = $this->searchExcerpt($result['summary'], $query);
        }

        // print_r($result);die();

        return $result;
    }

    private function _buildAssetDataForSearch($asset, $query)
    {
        $result = [
            'id' => $asset['id'],
            'score' => @$asset['searchScore'],
            'url' => $asset['url'],
            'title' => $asset['title'],
            'type' => 'assetVolume',
            'typeId' => $asset['volumeId'],
            'filterKey' => $this->_getFilterKey($asset),
            'summary' => null,
            'tags' => array(),
            'highlightedTitle' => $this->searchExcerpt($asset['title'], $query),
            'fileExtension' => $asset->getExtension(),
        ];

        // get tags
        $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');
        if ($searchTagFieldHandle && !empty($asset[$searchTagFieldHandle])) {
            foreach ($asset[$searchTagFieldHandle] as $tag) {
                $result['tags'][] = $tag['title'];
            }
        }

        // get summary
        $summaryObj = @$asset['summary'];
        if ($summaryObj && $summaryObj instanceof \craft\redactor\FieldData) {
            $result['summary'] = $summaryObj->__toString();
            $result['highlightedSummary'] = $this->searchExcerpt($result['summary'], $query);
        }

        return $result;
    }

    public function searchResults($query)
    {
        $results = [];
        $searchTagFieldHandle = $this->getConfig('searchTagFieldHandle');
        if (!empty($query) and !empty(trim($query))) {
            $entries = null;
            $searchableEntryTypeIds = $this->_getIdsForSearch('entryType');
            // print_r($searchableEntryTypeIds);
            if (!empty($searchableEntryTypeIds)) {
                $criteria = Entry::find();
                $criteria->search = trim($query);
                $criteria->limit = null;
                $criteria->orderBy = 'score';
                $criteria->typeId = $searchableEntryTypeIds;
                if ($searchTagFieldHandle) {
                    $criteria->with = [$searchTagFieldHandle];
                }
                $entries = $criteria->all();

                foreach ($entries as $entry) {
                    if($entry->id != 175517 && $entry->id != 178006){
                        $results[] = $this->_buildEntryDataForSearch($entry, $query);
                    }
                }
            }

            $assets = null;
            $searchableAssetVolumeIds = $this->_getIdsForSearch('assetVolume');
            // print_r($searchableAssetVolumeIds);
            if (!empty($searchableAssetVolumeIds)) {
                $criteria = Asset::find();
                $criteria->search = trim($query);
                $criteria->limit = null;
                $criteria->orderBy = 'score';
                $criteria->volumeId = $searchableAssetVolumeIds;
                if ($searchTagFieldHandle) {
                    $criteria->with = [$searchTagFieldHandle];
                }

                $assets = $criteria->all();

                foreach ($assets as $asset) {
                    $results[] = $this->_buildAssetDataForSearch($asset, $query);
                }
            }
            return $results;
        }
    }

    /**
     * DEPRECATED
     *
     * It's annoying that one of the filtering 'section' (resources) is part of 'pages', but different entry type, so we have to do some special things here to make it work. The trick is not to set 'section' as one of the criteria, but only set an array of 'entryTypes'.
     *
     * @param  [string] $query   [description]
     * @param  [array] $filters e.g. ['pages' => 1, 'resources' => 0, 'news' => 1]
     * @return [ElementCriteriaModel]          [description]
     */
    public function search($query, $filters)
    {

        if (!empty($query) and !empty(trim($query))) {
            $criteria = Entry::find();
            $criteria->search = trim($query);
            $criteria->limit = null;
            $criteria->orderBy = 'score';
            $includingEntryTypes = [];

            $pagesSection = Craft::$app->sections->getSectionByHandle('pages');
            $pagesEntryTypes = $pagesSection->getEntryTypes();
            foreach ($pagesEntryTypes as $entryType) {
                if (!empty($filters['pages'])) { // if including 'pages'
                    // if(empty($filters['schools'])) {
                    //     if($entryType->handle == 'school') {
                    //         continue; // skip school type
                    //     }
                    // }
                    $includingEntryTypes[] = $entryType->id;
                } else { // if not including normal 'pages'
                    // only include resource type
                    // if(!empty($filters['schools']) and $entryType->handle == 'school') {
                    //     $includingEntryTypes[] = $entryType->id;
                    // }
                }
            }

            // handle other types of sections
            foreach ($filters as $filterKey => $filterVal) {
                if (!empty($filterVal)) {
                    $section = Craft::$app->sections->getSectionByHandle($filterKey);
                    if ($section) {
                        $entryTypes = $section->getEntryTypes();
                        foreach ($entryTypes as $entryType) {
                            $includingEntryTypes[] = $entryType->id;
                        }
                    }
                }
            }

            // print_r($includingEntryTypes); die();

            if (!empty($includingEntryTypes)) {
                // the 'type' criteria doesn't support 'not' keyword. It only supports entry type handle, or id, or an array of those.
                $criteria->typeId = $includingEntryTypes;
            } else {
                // if no entryTypes included, means user unticked all filters in the front end, which should give no result. However, if we pass in empty array to ->type, it actually means: no type specified, search all entry types, which is not what we want. For an ugly workaround, we set the type to a random handle name, to make sure it will never get a result.
                $criteria->typeId = 'someTypeHandleWeWillNeverSet';
            }

            return $criteria;
        }
    }

    public function sanitisePhoneNumber($rawInput)
    {
        return preg_replace('/[^+\-0-9]/', '', $rawInput);
    }

    public function getTagsAsArray($tagQuery)
    {
        $result = [];
        if ($tagQuery) {
            foreach ($tagQuery->all() as $tag) {
                $result[$tag->id] = $tag->title;
            }
        }

        return $result;
    }

    /**
     * Get current user
     * @param [bool] $returnService
     * @return \craft\web\User if $returnService is true, otherwise (by default), \craft\element\User
     */
    public function getCurrentUser($returnService = false)
    {
        if ($returnService) {
            return Craft::$app->getUser();
        } else {
            return Craft::$app->getUser()->getIdentity();
        }
    }

    /**
     * @param $text
     * @return mixed|string
     */
    public function createJsFriendlySlug($text){
        $slug = $this->createSlug($text);
        $slug = str_replace(".", "", $slug); // js doesn't allow . in id, so we remove it
        $slug = "id-".$slug; //js does not allow id start with number, so we add a text in begin
        return $slug;
    }

    /**
     * Create Slug
     *
     * @param str text
     * @return str slug
     */
    public function createSlug($text)
    {
        return ElementHelper::normalizeSlug($text);
    }

    /**
     * Anchor Links - Generate an array of anchor links for the in-page navigation
     *
     * @param $entry
     * @return array Anchor Links
     */
    public function getAnchorLinks($entry)
    {

        $anchor_links = [];
        if ($entry instanceof \craft\elements\Entry) {
            $blocks = $entry['body']->all();
        } else {
            $blocks = null;
        }
        if (!is_array($blocks)) {
            $blocks = $blocks->all();
        }
        if ($blocks) {
            foreach ($blocks as $block) {
                foreach ($block->children->all() as $childBlock) {
                    if ($childBlock->type == 'anchorPoint' && $childBlock->label) {
                        $anchorLink = $this->createSlug($childBlock->label);
                        $anchor_links[] = ['link' => $anchorLink, 'label' => $childBlock->label];
                    } elseif ($childBlock->type == 'form' && $childBlock->heading) {
                        $anchorLink = $this->createSlug($childBlock->heading);
                        $anchor_links[] = ['link' => $anchorLink, 'label' => $childBlock->heading];
                    } elseif ($childBlock->type == 'tabs' && $childBlock->heading) {
                        $excludeAnchor = $childBlock->noAnchorpoint ?? false;
                        if (!$excludeAnchor) {
                            $anchorLink = $this->createSlug($childBlock->heading);
                            $anchor_links[] = ['link' => $anchorLink, 'label' => $childBlock->heading];
                        }
                    } elseif ($childBlock->type == 'glossary' && $childBlock->heading) {
                        $excludeAnchor = $childBlock->noAnchorpoint ?? false;
                        if (!$excludeAnchor) {
                            $anchorLink = $this->createSlug($childBlock->heading);
                            $anchor_links[] = ['link' => $anchorLink, 'label' => $childBlock->heading];
                        }
                    }
                }
            }
        }

        if (count($anchor_links) == 0) {
            return false;
        }

        return $anchor_links;
    }

    /**
     * Used by Nav cache and MainNav cache
     * @param  [type]  $settings   [description]
     * @param  boolean $forMainNav [description]
     * @return [type]              [description]
     */
    public function getFreshNavContent($settings, $forMainNav = false) {
        $query = \craft\elements\Entry::find()
                ->section('pages')
                ->limit(null)
                ->level(1);

        if($forMainNav) {
            $query->hideFromNavigation("not 1");
        }

        $topLevelPages = $query->all();

        $navItems = $this->_getNavItems($topLevelPages, null, $forMainNav);

        // if including homepage, prepend homepage to the front of the list
        if(!empty($settings['includeHome'])) {
            $homepage = \craft\elements\Entry::find()
                    ->section('homepage')
                    ->one();
            if($homepage) {
                $homepageNavItem = [
                    'id' => $homepage->id,
                    'title' => $homepage->title,
                    'url' => '/',
                    'target' => null,
                    'level' => 1,
                    'parentId' => null,
                    'descendantsIds' => array(),
                    'childrenIds' => array(), // useful to determine the active state of an item in nav
                    'children' => null, // this can be null or a not empty array
                    // 'ancestors' => array(), // array, list of ancestors of this entry
                ];
                $navItems = array_merge([$homepageNavItem], $navItems);
            }
        }

        return json_encode($navItems
            , JSON_PRETTY_PRINT // for debugging purpose only, so we can have the JSON more human readable.
        );
    }

    /**
     * Recursively get all the navItems (in pages section).
     *
     * Please note, any pages with 'hideFromNavigation' turned on will have itself and its children excluded from the navItems.
     * 
     * @param  array of Entries $entries If passing ElementQuery / EntryQuery directly, it's probably gonna work as well, because ElementQuery is iterable? But seems like all Craft docs have ->all() / .all() function called. So we can assume it's a good practice to do so, because it might have some benefits, e.g. caching?
     * @return array  array of the navItems on this level. It's unlikely that this function would return empty array.
     */
    private function _getNavItems($entries, $parentId = null, $forMainNav = false) {
        $navItems = [];
        foreach ($entries as $entry) {
            // get the meta of the navItem itself
            $navItem = [
                'id' => YumpModule::$instance->yump->getEntryId($entry),
                'title' => $entry->title,
                'url' => $entry->getUrl(),
                'target' => null,
                'level' => $entry->level,
                'parentId' => $parentId,
                'descendantsIds' => array(), // used to check the active state of an entry (an entry is marked as active if it's current or is ancestor of current entry)
                'childrenIds' => array(), // used to find the parent page of an entry
                'children' => null, // this can be null or a not empty array
                // 'ancestors' => $ancestorList, // array, list of ancestors of this entry
            ];
            if($entry->getType()->handle == 'linkOnly') {
                $link = $entry->cta;
                $navItem['url'] = YumpModule::$instance->yump->generateValidUrl($link->getUrl());
                $navItem['target'] = $link->getTarget();
            }

            // get descendantsIds
            $decendants = $entry->getDescendants()->limit(null);
            if($forMainNav) {
                $decendants->hideFromNavigation("not 1");
            }

            $navItem['descendantsIds'] = $decendants->ids();

            // get the children of the navItem
            $children = $entry->getChildren()
                        ->limit(null);
            if($forMainNav) {
                $children->hideFromNavigation("not 1");
            }

            if($children->count() !== 0) {
                $navItem['childrenIds'] = $children->ids();
                $navItem['children'] = $this->_getNavItems($children->all(), $navItem['id'], $forMainNav); // call this function recursively to get children navItems.
            }

            // add it to the navItems list
            $navItems[] = $navItem;
        }
        return $navItems;
    }

}
