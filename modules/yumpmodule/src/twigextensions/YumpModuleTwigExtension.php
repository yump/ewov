<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\twigextensions;

use modules\yumpmodule\YumpModule;

use Craft;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class YumpModuleTwigExtension extends \Twig\Extension\AbstractExtension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'YumpModule';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig\TwigFilter('someFilter', [$this, 'someInternalFunction']),
            new \Twig\TwigFilter('searchExcerpt', [$this, 'searchExcerpt']),
            new \Twig\TwigFilter('getExcerpt', [$this, 'getExcerpt']),
        ];
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
    * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig\TwigFunction('someFunction', [$this, 'someInternalFunction']),
            new \Twig\TwigFunction('is_array', [$this, 'isArray']),
        ];
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
     * @param null $text
     *
     * @return string
     */
    public function someInternalFunction($text = null)
    {
        $result = $text . " in the way";

        return $result;
    }

    /**
     * The following two functions regarding getting excerpt might need to be tweaked (it doesn't work very well)
     * @param  [type]  $text   [description]
     * @param  [type]  $phrase [description]
     * @param  integer $radius [description]
     * @param  string  $ending [description]
     * @return [type]          [description]
     */
    public function searchExcerpt($text, $phrase, $radius = 150, $ending = "..."){
        return YumpModule::$instance->yump->searchExcerpt($text, $phrase, $radius, $ending);
    }

    public function getExcerpt($text, $options = array()) {
        $options = array_merge(array(
            'startPos' => 0,
            'maxLength' => 150,
        ), $options);

        return YumpModule::$instance->yump->getExcerpt($text, $options['startPos'], $options['maxLength']);
    }

    public function isArray($obj) {
        return is_array($obj);
    }
}
