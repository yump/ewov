<?php
/**
 * This class is used to manage the cache of the site navigation.
 *
 * The key part is to define how do we get a fresh list of nav items.
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\gears\cache\adapters;

use modules\yumpmodule\YumpModule;
use modules\yumpmodule\gears\Cache as CacheGears;

class Nav extends CacheGears
{
	/**
	 * In case we need certain custom settings for getFreshContent() method
	 * @var [type]
	 */
	private $_settings;

	public function __construct($settings = array()) {
		$cacheKey = YumpModule::$instance->yump->getConfig('siteNavigationCacheKey') ?: 'site-navigation'; // use 'siteNavigationCacheKey' in general.php or use 'site-navigation' as the cacheKey by default

		parent::__construct($cacheKey
			, CacheGears::CACHE_METHOD_YUMP // By default it uses Yump cache. If you want to use Craft cache instead, do it here
			, true // returnAsArray
		);

		// $this->setReturnedAsArray(true);

		$this->_settings = $settings;
	}

	public function getFreshContent() {
		return YumpModule::$instance->yump->getFreshNavContent($this->_settings);
	}

	
}