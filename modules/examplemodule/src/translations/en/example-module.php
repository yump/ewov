<?php
/**
 * example module for Craft CMS 3.x
 *
 * Just an example to show how to create a new module and extend a few things from the yumpmodule.
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

/**
 * example en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('example-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Yump
 * @package   ExampleModule
 * @since     1.0.0
 */
return [
    'example plugin loaded' => 'example plugin loaded',
];
