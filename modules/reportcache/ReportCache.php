<?php

namespace modules\reportcache;

use Craft;
use putyourlightson\blitz\services\ExpireCacheService;
use putyourlightson\blitz\models\SiteUriModel;

//for on Elements::EVENT_AFTER_SAVE
use craft\services\Elements;
use craft\elements\Entry;
use craft\events\ModelEvent;
use craft\helpers\ElementHelper;
use Seld\PharUtils\Timestamps;
use yii\base\Event;

/**
 * Custom module class.
 *
 * This class will be available throughout the system via:
 * `Craft::$app->getModule('my-module')`.
 *
 * You can change its module ID ("my-module") to something else from
 * config/app.php.
 *
 * If you want the module to get loaded on every request, uncomment this line
 * in config/app.php:
 *
 *     'bootstrap' => ['my-module']
 *
 * Learn more about Yii module development in Yii's documentation:
 * http://www.yiiframework.com/doc-2.0/guide-structure-modules.html
 */
class ReportCache extends \yii\base\Module
{
    const REPORT_LANDING_ENTRY_TYPE_HANDLE = "reportsLanding";
    const REPORT_SECTION_HANDLE = "reports";
    const PAGE_SECTION_HANDLE = "pages";
    /**
     * Initializes the module.
     */
    public function init()
    {
        // Set a @modules alias pointed to the modules/ directory
        Craft::setAlias('@modules', __DIR__);

        // Set the controllerNamespace based on whether this is a console or web request
        if (Craft::$app->getRequest()->getIsConsoleRequest()) {
            $this->controllerNamespace = 'modules\\console\\controllers';
        } else {
            $this->controllerNamespace = 'modules\\controllers';
        }

        parent::init();

        // Custom initialization code goes here...
        $this->afterSaveReportEntry();
    }


    private function afterSaveReportEntry():void
    {
        Event::on(
            Elements::class,
            Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (\craft\events\ElementEvent $event) {
                $element = $event->element;
                if (
                    $element instanceof Entry // is entry
                    && $element->getSection()?->handle == self::REPORT_SECTION_HANDLE // is in 'pages' section
                    && !$element->getIsRevision() // is not revision
                    && !$element->getIsDraft() // is not draft
                    && !$element->propagating // not propagating (avoid batch propagating)
                    && !$element->resaving // not resaving (avoid batch resaving)
                ) {
                    Craft::error("Report entry saved: " . $element->status, __METHOD__);
                    Craft::error("Report type: " . $element->reportType->ids()[0], __METHOD__);
                    $landingPages = $this->getAllParentPagesByReportType($element);

                    if(sizeof($landingPages) > 0) {
                        $blitzCacheService = new ExpireCacheService();
                        array_map(function($landingpage) use ($blitzCacheService, $element) {
                            if(!$landingpage instanceof Entry) { return; }
                            $uriModel = new SiteUriModel();
                            $uriModel->uri = $landingpage->uri;
                            $uriModel->siteId = $landingpage->siteId;

                            // && $element->dateCreated == $element->postDate
                            if($element->status == $element::STATUS_PENDING) {
                                Craft::info("Report entry set post date in future: " . $element->postDate->format("Y M d h i s"), __METHOD__); // tests
                                $blitzCacheService->expireUris([$uriModel], $element->postDate);
                            // } else {
                            //     Craft::info("Report entry clear blitz cache now", __METHOD__); // tests
                                // $blitzCacheService->expireUris([$uriModel]);
                            }
                        }, $landingPages);
                    }
                }
            }
        );
    }

    private function getAllParentPagesByReportType(Entry $reportEntry): array
    {
        return Entry::find()
            ->section(self::PAGE_SECTION_HANDLE)
            ->type(self::REPORT_LANDING_ENTRY_TYPE_HANDLE)
            ->relatedTo($reportEntry->reportType->ids())
            ->all();
    }
}
