<?php
/**
 * Created by PhpStorm.
 * User: yuxingw
 * Date: 6/12/18
 * Time: 12:52 PM
 */

namespace modules\factsheets\services;

class Logs
{
    //log settings
    CONST LOG_PATH = CRAFT_BASE_PATH . '/storage/logs/';
    CONST EXTENSION = '.log';

    //set message level
    CONST LEVEL_INFO = 'info';
    CONST LEVEL_WARNING = 'warning';
    CONST LEVEL_ERROR = 'error';

    //set frequency
    CONST FREQUENCY_NONE = 0;
    CONST FREQUENCY_DAILY = 1;
    CONST FREQUENCY_MONTHLY = 2;
    CONST FREQUENCY_YEARLY= 3;

    //set log names
    CONST NAME_DEFAULT = "factsheets";

    static function log($message, $type = self::LEVEL_INFO, $filename = self::NAME_DEFAULT, $frequency = self::FREQUENCY_NONE)
    {
        //create folder path if not existed
        if (!file_exists(self::LOG_PATH)) {
            mkdir(self::LOG_PATH, 0755, true);
        }

        //set log frequency
        switch ($frequency){
            case 0:
                $dateString = "";
                break;
            case 1:
                $dateString = "_".date("ymd", time());
                break;
            case 2:
                $dateString = "_".date("ym", time());
                break;
            case 3:
                $dateString = "_".date("y", time());
                break;
            default:
                $dateString = "";
                break;
        }

        $filePath = self::LOG_PATH. $filename . $dateString . self::EXTENSION;
        $date = date('Y-m-d H:i:s', time());
        $messageType = gettype($message);

        //process $message based on the value type given
        switch ($messageType){
            case "array":
                $message = print_r($message,1);
                break;
            case "object":
                $message = print_r($message,1);
                break;
            case "resource":
                $message = "resource type log is not supported";
                break;
            case "unknown type":
                $message = "unknown type detected usually means variable is unset";
                break;
            case "string":
                $array = json_decode($message);
                //if can be json processed, and result is array or object, we json decode string before log it
                if((is_array($array) || is_object($array)) && json_last_error() == JSON_ERROR_NONE){
                    $message = print_r(json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE), true);
                }
                break;
            default:
                //process boolean,integer,double,NULL
                break;
        }

        //construct the final message and log it
        error_log("[$date][".__METHOD__."][$type][$messageType] ".$message.PHP_EOL, 3, $filePath);
    }

    static function error($message, $filename = self::NAME_DEFAULT){
        self::log($message, self::LEVEL_ERROR, $filename);
    }

    static function warning($message, $filename = self::NAME_DEFAULT){
        self::log($message, self::LEVEL_WARNING, $filename);
    }

    static function info($message, $filename = self::NAME_DEFAULT){
        self::log($message, self::LEVEL_INFO, $filename);
    }

    static function logDaily($message, $filename = self::NAME_DEFAULT, $type = self::LEVEL_INFO){
        self::log($message, $type, $filename, self::FREQUENCY_DAILY);
    }

    static function logMonthly($message, $filename = self::NAME_DEFAULT, $type = self::LEVEL_INFO){
        self::log($message, $type, $filename, self::FREQUENCY_MONTHLY);
    }

    static function logYearly($message, $filename = self::NAME_DEFAULT, $type = self::LEVEL_INFO){
        self::log($message, $type, $filename, self::FREQUENCY_YEARLY);
    }
}
