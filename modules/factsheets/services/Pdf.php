<?php
namespace modules\factsheets\services;

use Craft;
use craft\helpers\DateTimeHelper;
use craft\web\View;
use Dompdf\Dompdf;
use Dompdf\Options;

class Pdf {

    public $debug = false;
    public $pdfPath = CRAFT_BASE_PATH."/assets/factsheets/"; //make sure add this folder to .gitignore
    public $templateUrl = 'fact-sheets/_pdf';

    /**
     * trigger a pdf download in browser
     * @param $entry
     * @return \craft\web\Response|void|\yii\console\Response
     */
    public function downloadPdfByEntry($entry){
        $filename = $this->loadPdfFileByEntry($entry);
        $handle = fopen($this->pdfPath . $filename, 'rb');//open pdf
        try {
            return Craft::$app->getResponse()->sendStreamAsFile($handle,  $filename, ['mimeType'=> 'application/pdf', 'inline'=> 0]); //render pdf
        }catch (\yii\web\RangeNotSatisfiableHttpException $e){
            Craft::error($e);
        }
    }

    /**
     * Stream saved static pdf on browser
     * Auto-handles missing pdf file
     *
     * @param $entry
     * @return \craft\web\Response|void|\yii\console\Response
     */
    public function streamPdfByEntry($entry) {
        $filename = $this->loadPdfFileByEntry($entry);
        $handle = fopen($this->pdfPath . $filename, 'rb');//open pdf
        try {
            return Craft::$app->getResponse()->sendStreamAsFile($handle,  $filename, ['mimeType'=> 'application/pdf', 'inline'=> 1]); //render pdf
        }catch (\yii\web\RangeNotSatisfiableHttpException $e){
            Craft::error($e);
        }
    }

    /**
     * Put html to dompdf, and returns the html from dompdf
     * This is used for debug purpose, to visualise html parsed by dompdf
     *
     * @param $entry
     * @return string
     */
    public function getDomPdfHtmlByEntry($entry):string{
        $html = $this->_renderHtmlByEntry($entry);
        return $this->_htmlToPdf($html)->outputHtml();
    }

    public function getPdfSizeByEntry($entry) {
        $filename = $this->loadPdfFileByEntry($entry);
        return filesize($this->pdfPath. $filename);
    }

    /**
     * For craft queue jobs
     * Save a physical pdf to a folder
     * pdf will be named by ${entry->slug}.pdf
     * @param $entry
     */
    public function resavePdfByEntry($entry) {
        $filename = isset($entry->slug)?$entry->slug:"fact-sheets-".date("m-d");
        $filename = $filename . ".pdf";

        $html = $this->_renderHtmlByEntry($entry);
        $domPdf = $this->_htmlToPdf($html);
        $domPdf->render();
        $this->savePdfAsFile($domPdf, $filename);
    }

    /**
     * Load static pdf file based on entry
     * if not exist we create a new file
     *
     * @param $entry
     * @return string
     */
    private function loadPdfFileByEntry($entry):string {
        $filename = isset($entry->slug)?$entry->slug:"fact-sheets-".date("m-d");
        $filename = $filename . ".pdf";

        //if entry does not have associated pdf file exists we create a new one
        if(!file_exists($this->pdfPath. $filename)){
            $html = $this->_renderHtmlByEntry($entry);
            $domPdf = $this->_htmlToPdf($html);
            $domPdf->render();
            $this->savePdfAsFile($domPdf, $filename);
        }

        return $filename;
    }

    /**
     * Craft render html by entry, we are using it to get processed html from entry
     * @param $entry
     * @return string|void
     */
    private function _renderHtmlByEntry($entry){
        try {
            return Craft::$app->view->renderTemplate($this->templateUrl, ['entry'=>$entry], View::TEMPLATE_MODE_SITE);
        }catch (\Exception $e){
            Craft::error($e);
        }
    }

    /**
     * Convert HTML to pdf using domPDF
     * @param string $html
     * @return Dompdf $domPdf
     */
    private function _htmlToPdf(string $html):Dompdf{
        //the following three steps are crucial for images to show up in domPDF
        $options = new Options();
        $options->setFontDir(Craft::getAlias('@webroot/assets/fonts')); // set font folder
        $options->setIsRemoteEnabled(true); // for png, jpg to render in pdf
        $options->setLogOutputFile(Craft::getAlias('@webroot/storage/logs/factsheets.log')); // set log location
        $options->setIsHtml5ParserEnabled(true); //don't know, just turn it on
        $options->setDpi(174);//set page to be as wide as 1440px, default is 96 dpi which is 900pxish width
        $options->setFontHeightRatio(1);//default 1.1
        $options->setDefaultMediaType('print');//using @page query from @media print {}, to allow page break
//        $options->setIsJavascriptEnabled(true);//enable javascript if needed

        //uncomment to debug
//        $options->setDebugLayout(true); //recommend using for item alignment
//        $options->setDebugLayoutInline(true);
//        $options->setDebugLayoutBlocks(true);
//        $options->setDebugLayoutPaddingBox(true);
//        $options->setDebugLayoutLines(true);
//        $options->setDebugPng(true);
//        $options->setDebugCss(true);

        $this->_stripHtmlTagsForPdf($html);
        $this->_applyFullUrlForPdf($html);
        //todo: support svg https://github.com/dompdf/dompdf#limitations-known-issues

        $domPDF = new Dompdf($options);
//        if($this->debug) { global $_dompdf_warnings, $_dompdf_warnings; }// print dom pdf rendering error, first part
        $domPDF->loadHtml($html);
//        if($this->debug) { Logs::error($_dompdf_warnings); } // print dom pdf rendering error, second part
        $domPDF->setPaper('A4', 'portrait');
        return $domPDF;
    }

    /**
     * Due to pdf doesn't work with local assets.
     * This function changes url like /assets/img/xxx.jpg to https://live-url.com.au/assets/img/xxx.jpg
     * Live url is defined by getenv('DEFAULT_SITE_URL')
     *
     * @param string $html
     */
    private function _applyFullUrlForPdf(string &$html){
        $html = str_replace('href="/', 'href="'. getenv('DEFAULT_SITE_URL').'/', $html);
        $html = str_replace("href='/", "href='". getenv('DEFAULT_SITE_URL').'/', $html);
        $html = str_replace("url('/", "url('". getenv('DEFAULT_SITE_URL').'/', $html);
        $html = str_replace('src="/', 'src="'. getenv('DEFAULT_SITE_URL').'/', $html);
        $html = str_replace("src='/", "src='". getenv('DEFAULT_SITE_URL').'/', $html);
    }

    /**
     * clean html for pdf, remove tags including 'id="xxx"','aria-label="xxx"','aria-hidden="xxx"','tabindex="xxx"'
     * replace <section> <figure> to <div>
     * @param $html
     */
    private function _stripHtmlTagsForPdf(&$html){
        $html = str_replace('  ', ' ', $html);//reduce the amount of space
        $html = str_replace(' "', '"', $html);//dom pdf don't like empty space at the end of class, e.g. //class="class1 "
        $this->_replaceDomElementTagWithDiv($html, 'section');//dompdf hates section
        $this->_replaceDomElementTagWithDiv($html, 'figure');//dompdf hates figure
        $this->_removeDomElementAttributes($html, ['id', 'aria-label', 'aria-hidden', 'tabindex']);//remove hates all these
    }

    /**
     * if attributes = ['id']
     * remove string like 'id="xxx"' in html
     *
     * @param string $html
     * @param array $attributes
     */
    private function _removeDomElementAttributes(string &$html, array $attributes = []):void{
        $html = preg_replace('#\s('.implode('|',$attributes).')="[^"]+"#', '', $html);
    }

    /**
     * replace special tags with div in html
     * e.g. <figure>...</figure> will be replaced by <div></div>
     * @param string $html
     * @param string $original
     */
    private function _replaceDomElementTagWithDiv(string &$html, string $original):void{
        $html = str_replace('<'.$original.'', '<div', $html);
        $html = str_replace('</'.$original.'>', '</div>', $html);
    }

    /**
     * save a physical pdf file by provided pdfPath
     * @param Dompdf $DomPdf
     * @param string $filename //with .pdf extension
     */
    private function savePdfAsFile(Dompdf $DomPdf, string $filename):void
    {
        if(!is_dir($this->pdfPath)) mkdir($this->pdfPath, 0775);//create folder if not exist;
        $savePath = $this->pdfPath . $filename;
        $output = $DomPdf->output();
        file_put_contents($savePath, $output);
    }
}
