<?php


namespace modules\factsheets\controllers;

use Craft;
use craft\web\Controller;
use craft\web\Request;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class BaseWebController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        if(getEnv("ENVIRONMENT") === "dev") {
            $this->enableCsrfValidation = false;
        }

        parent::__construct($id, $module, $config);
    }

    /**
     * @throws ForbiddenHttpException
     */
    protected function handleForbiddenResponse(){
        throw new ForbiddenHttpException('User is not authorized to perform this action');
    }

    /**
     * @throws NotFoundHttpException
     */
    protected function handleNotFoundResponse(){
        throw new NotFoundHttpException();
    }

    /**
     * @throws BadRequestHttpException
     */
    protected function handleBadRequestResponse(){
        throw new BadRequestHttpException();
    }

    /**
     * set craft cms error
     *
     * @param $error
     */
    protected function setError($error){
        try{
            Craft::$app->getSession()->setError($error);
        }catch (\craft\errors\MissingComponentException $e){
            Craft::error($e, __METHOD__);
        }
    }

    /**
     * Require allowed ip or admin
     * @param $request
     */
    protected function requireAllowedIP(Request $request){
        $isAdmin = Craft::$app->getUser()->getIsAdmin();
        if(!in_array($request->getRemoteIP(), Variable::$allowIPAddress) && !$isAdmin){
            try{
                $this->handleForbiddenResponse();
            }catch (ForbiddenHttpException $e){
                Craft::error($e->getMessage(), __METHOD__);
            }
        }
    }

    /**
     * @param $name
     * @param null $default
     * @return mixed
     */
    protected function getParam($name, $default = null){
        return Craft::$app->request->getParam($name, $default);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getParams(){
        $request = Craft::$app->getRequest();
        if(!empty($request->getBodyParams())) return $request->getBodyParams();
        if(!empty($request->getQueryParams())) return $request->getQueryParams();
        return [];
    }
}
