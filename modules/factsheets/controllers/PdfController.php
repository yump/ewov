<?php

namespace modules\factsheets\controllers;

use Craft;
use craft\elements\Entry;
use craft\web\View;
use craft\web\Response;
use modules\factsheets\services\Logs;
use modules\factsheets\services\Pdf;

class PdfController extends BaseWebController {

    protected array|int|bool $allowAnonymous = ['download','stream','get-size'];

    private $pdfService;

    public function __construct($id, $module, $config = [])
    {
        $this->pdfService = new pdf();
        parent::__construct($id, $module, $config);
    }

    // /actions/factsheets/pdf/pdf?id=127455
    /**
     *
     * @return Response
     */
    public function actionDownload():Response{
        $entry = $this->_getEntryByIdParam();
        return $this->pdfService->downloadPdfByEntry($entry);
    }

    public function actionStream():Response {
        $entry = $this->_getEntryByIdParam();
        return $this->pdfService->streamPdfByEntry($entry);
    }

    /**
     * @return string
     */
    public function actionGetSize():string{
        $entry = $this->_getEntryByIdParam();
        $filesize = $this->pdfService->getPdfSizeByEntry($entry);
        return (string)round($filesize / 1024 / 1024, 2);
    }

    // /actions/factsheets/pdf/pdf-html?id=127455
    /**
     * Run entry's html through domPdf, and return html processed by domPDF for debug purpose
     *
     * @return string
     */
    public function actionHtml():string {
        $entry = $this->_getEntryByIdParam();
        $html = $this->pdfService->getDomPdfHtmlByEntry($entry);
        if(file_exists(Craft::getAlias('@webroot/storage/logs/html.log'))) unlink(Craft::getAlias('@webroot/storage/logs/html.log'));//delete the log fine, if exists
        Logs::log($html, 'info', 'html');//write a new one
        return $html;
    }

    /**
     * @return array|\craft\base\ElementInterface|Entry|null
     */
    private function _getEntryByIdParam(){
        $elementId = (int)$this->getParam('id'); //convert to (int) to filter out potential malicious code
        return Entry::find()->id($elementId)->status(null)->one();
    }
}
