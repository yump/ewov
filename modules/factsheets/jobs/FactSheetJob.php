<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace modules\factsheets\jobs;

use craft\elements\Entry;
use craft\queue\JobInterface;
use craft\queue\QueueInterface;
use modules\factsheets\services\Logs;
use modules\factsheets\services\Pdf;
use yii\queue\Queue;

/**
 * JobInterface defines the common interface to be implemented by job classes.
 * A class implementing this interface should also use [[SavableComponentTrait]] and [[JobTrait]].
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.0.0
 */
class FactSheetJob implements JobInterface
{
    public $entryId;

    public function __construct($entryId){
        $this->entryId = $entryId;
    }
    /**
     * Returns the description that should be used for the job.
     *
     * @return string
     */
    public function getDescription():string{
        return 'Refresh generate fact sheet pdf';
    }


    /**
     * @param QueueInterface|Queue $queue
//     * @return array|mixed|void
     */
    public function execute($queue): void{
//        Logs::error('job queue executed');
        $entry = Entry::find()->id($this->entryId)->status(null)->one();
        (new Pdf())->resavePdfByEntry($entry);
    }
}
