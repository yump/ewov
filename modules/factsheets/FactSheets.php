<?php
/**
 * example module for Craft CMS 3.x
 *
 * Just an example to show how to create a new module and extend a few things from the yumpmodule.
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\factsheets;

use Craft;
use craft\elements\Entry;
use craft\events\ElementEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\queue\Queue;
use craft\services\Elements;
use craft\web\View;
use craft\console\Application as ConsoleApplication;

use modules\factsheets\services\Logs;
use modules\factsheets\jobs\FactSheetJob;
use yii\base\Event;
use yii\base\Module;
use Yii;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Yump
 * @package   ExampleModule
 * @since     1.0.0
 *
 */
class FactSheets extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * ExampleModule::$instance
     *
     * @var FactSheets
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/factsheets', $this->getBasePath());
        $this->controllerNamespace = 'modules\factsheets\controllers';

        // Base template directory
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $e) {
            if (is_dir($baseDir = $this->getBasePath().DIRECTORY_SEPARATOR.'templates')) {
                $e->roots[$this->id] = $baseDir;
            }
        });

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * ExampleModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        // Add in our console commands
        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'modules\factsheets\console\controllers';
        }

        $this->_updateFactSheets();
    }

    // Protected Methods
    // =========================================================================

    private function _updateFactSheets(){
        Event::on(
            Elements::class,
            Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (ElementEvent $event) {
                $element = $event->element;
                if($element instanceof Entry
                    && !$element->getIsRevision() // is not revision
                    && !$element->getIsDraft() // is not draft
                    && !$element->propagating // not propagating (avoid batch propagating)
                    && !$element->resaving // not resaving (avoid batch resaving)10/);
                ){
                    //add it to job queue instead of execute it right away;
                    if($element->getSection()->handle == 'factSheets'){
                        //(new Queue())->push(new FactSheetJob($element->id));
                        Yii::$app->queue->push(new FactSheetJob($element->id));
                    }
                }
            }
        );
    }
}
