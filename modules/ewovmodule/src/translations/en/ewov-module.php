<?php
/**
 * EWOV module for Craft CMS 3.x
 *
 * Custom EWOV Functionality
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

/**
 * EWOV en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('ewov-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Yump
 * @package   EWOVModule
 * @since     1.0.0
 */
return [
    'EWOV plugin loaded' => 'EWOV plugin loaded',
];
