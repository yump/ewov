<?php
/**
 * EWOV module for Craft CMS 3.x
 *
 * Custom EWOV Functionality
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\ewovmodule\variables;

use modules\ewovmodule\EWOVModule;

use Craft;

/**
 * EWOV Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.eWOVModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Yump
 * @package   EWOVModule
 * @since     1.0.0
 */
class EWOVModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Yump Special:
     * If function in this class not found, go find in in service class
     */
    function __call($method, $arguments) {
        return call_user_func_array(array(EWOVModule::$instance->ewov, $method), $arguments);
    }


    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.eWOVModule.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.eWOVModule.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
