<?php
/**
 * EWOV module for Craft CMS 3.x
 *
 * Custom EWOV Functionality
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\ewovmodule\services;

use craft\elements\Entry;
use modules\ewovmodule\EWOVModule;

use Craft;
use craft\base\Component;
use modules\yumpmodule\YumpModule;
use modules\yumpmodule\gears\cache\adapters\Nav as YumpNavAdapter;

/**
 * EWOVModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   EWOVModule
 * @since     1.0.0
 */
class EWOVModuleService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     EWOVModule::$instance->eWOVModuleService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }

    /**
     * Get latest News Items
     *
     * @return [array] of News entries
     */    
    public function getLatestNews()
    {
        $newsEntries = Entry::find()
            ->section('news')
            ->orderBy('postDate desc')
            ->limit(3)
            ->all();

        return $newsEntries;
    }

    public function getTopNav($topLevelEntries) {

		$navItems = $this->_getNavItems($topLevelEntries);        

        return $navItems;
    }

    /**
	 * Recursively get all the navItems (in pages section).
	 *
	 * Please note, any pages with 'hideFromNavigation' turned on will have itself and its children excluded from the navItems.
	 * 
	 * @param  array of Entries $entries If passing ElementQuery / EntryQuery directly, it's probably gonna work as well, because ElementQuery is iterable? But seems like all Craft docs have ->all() / .all() function called. So we can assume it's a good practice to do so, because it might have some benefits, e.g. caching?
	 * @return array  array of the navItems on this level. It's unlikely that this function would return empty array.
	 */
	private function _getNavItems($entries, $parentId = null) {
		$navItems = [];
		$entriesItems = $entries;
		if(!is_array($entries)){
            $entriesItems = $entries->all();
        }
		foreach ($entriesItems as $entry) {
			// get the meta of the navItem itself
			$navItem = [
				'id' => YumpModule::$instance->yump->getEntryId($entry),
				'title' => $entry->title,
				'url' => $entry->getUrl(),
				'target' => null,
				'level' => $entry->level,
				'parentId' => $parentId,
				'descendantsIds' => array(), // used to check the active state of an entry (an entry is marked as active if it's current or is ancestor of current entry)
				'childrenIds' => array(), // used to find the parent page of an entry
				'children' => null, // this can be null or a not empty array
				// 'ancestors' => $ancestorList, // array, list of ancestors of this entry
			];
			if($entry->getType()->handle == 'linkOnly') {
				$link = $entry->cta;
				$navItem['url'] = YumpModule::$instance->yump->generateValidUrl($link->getUrl());
				$navItem['target'] = $link->getTarget();
			}

			// get descendantsIds
			$navItem['descendantsIds'] = $entry->getDescendants()
										->limit(null)->ids();

			// get the children of the navItem
			$children = $entry->getChildren()
						->limit(null)
                        ->level(2);
			if($children->count() !== 0) {
				$navItem['childrenIds'] = $children->ids();
				$navItem['children'] = $this->_getNavItems($children->all(), $navItem['id']); 
			}

			// add it to the navItems list
			$navItems[] = $navItem;
		}
		return $navItems;
	}

	public function findReportLandingPageByEntry($entry) {
		$reportType = $entry->reportType->one();
		return $this->findReportLandingPageByType($reportType);
	}

	public function findReportLandingPageByType($type) {
		if($type) {
			return Entry::find()
            ->section('pages')
            ->type('reportsLanding')
            ->reportType($type)
            ->one();
		}
	}

}
