<?php
/**
 * Created by PhpStorm.
 * User: Parry
 * Date: 8/11/2018
 * Time: 3:38 PM
 */

namespace modules\ewovmodule\services;
use Craft;
use craft\base\Component;
use craft\helpers\FileHelper;
use yii\helpers\BaseJson as Json;

class Logs extends Component
{
    /**
     * @param $message
     * @param $method
     * @param string $folder
     */
    public static function log($message, $method = __METHOD__, $folder = "/ewov.log")
    {
        try{
            $dateTime = new \DateTime();
        }catch (\Exception $e){
            Craft::error($e);
        }
        $type = explode('::', $method)[1];


        $options = Json::encode([
            'date' => $dateTime->format('Y-m-d H:i:s'),
            'message' => $message,
            'type' => $type,
        ]);

        try{
            $fp = fopen( Craft::$app->path->getLogPath() . $folder, 'ab');
            fwrite($fp, $options . PHP_EOL);
            fclose($fp);
        }catch (\yii\base\Exception $e){
            Craft::error($e);
        }
    }

    public static function frequentLog($message, $method = __METHOD__, $folder = "/ewov")
    {
        $dateTime = new \DateTime();
        $type = explode('::', $method)[1];

        $options = Json::encode([
            'date' => $dateTime->format('Y-m-d H:i:s'),
            'message' => $message,
            'type' => $type,
        ]);

        $index = date("Ym", time());
        $fp = fopen( Craft::$app->path->getLogPath() . $folder."_$index.log", 'ab');
        fwrite($fp, $options . PHP_EOL);
        fclose($fp);
    }

    public static function testLog($message, $method = __METHOD__, $folder="/ewov.log"){
        $dateTime = new \DateTime();
        $type = explode('::', $method)[1];
        $options = "[[".Json::encode($dateTime)."]] Message:: ".print_r($message,1)."| Type::".Json::encode($type);

        $fp = fopen( Craft::$app->path->getLogPath() . $folder, 'ab');
        fwrite($fp, $options . PHP_EOL);
        fclose($fp);
    }

    public function clear()
    {
        if (@file_exists(Craft::$app->path->getLogPath() . '/ewov.log')) {
            FileHelper::unlink(Craft::$app->path->getLogPath() . '/ewov.log');
        }
    }

    /**
     * Checks the script runtime in millisecond, use as sample.
     *
     * $start = microtime()
     * $end = microtime()
     * Logs::log("loop takes ". Log::runtime($end, $start). " ms");
     *
     * @param $end
     * @param $start
     * @return float|int ms
     */
    public static function runtime($end, $start) {
        return $end - $start;
    }
}
