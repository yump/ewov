/**
 * EWOV module for Craft CMS
 *
 * EWOVModuleWidget Widget JS
 *
 * @author    Yump
 * @copyright Copyright (c) 2020 Yump
 * @link      https://yump.com.au
 * @package   EWOVModule
 * @since     1.0.0
 */
