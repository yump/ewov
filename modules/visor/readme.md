# Introduction
This module can be used for both craft 3 and craft 4 to apply visor. 
This module will work around Blitz and loading via ajax.
This module does not include any database updates to current craft install.

## Installation
1. Copy and paste visor folder into ./modules/ folder to any craft install
2. Copy and paste ./visor/_scripts into your web root folder e.g. "/assets/js/visor/*"
3. Load the module into craft environment: 
   1. Add module into ./config/app.php
   ```
      'modules' => [
           ...
           'visor' => \modules\visor\Visor::class,
       ],
      'bootstrap' => [ ... ,'visor'],
   ```
   2. Load it in composer: add this into composer.json at 
   ```
        "autoload": {
          "psr-4": {
             ...
             "modules\\visor\\": "modules/visor/"
          }
        },
   ```
   3. To confirm it's successfully loaded, please visit "`/actions/visor/default/access`" see if it's loaded a style less a visor icon.
4. Include this visor.js to layout template .e.g. base.twig or layout.twig '<scirpt src="{{ url('/assets/js/visor/visor.js') }}"></script>'

### References
./visor/visor.zip is the original zip file for this plugin, this module was directly developed based on this. 
If you are interested in update any styles, please refer to the original plugins. 

### Craft 3 project
- updates and fixes, remove "array|bool|int" at "visor/controllers/DefaultController.php:33"
