<?php
/**
 * Custom Configuration
 *
 * For Craft config settings, see: general.php
 *
 * Settings here can be accessed by: Craft::$app->config->custom->{mycustomsetting}
 */

return [
    // Global settings
    '*' => [
        'yumpCurlCacheFolder' => CRAFT_BASE_PATH . '/yump/private/cache/curl',

        // custom script permission control
        'controllerActionsPassword' => 'QUpX67EkiETU7Rg',
        'controllerActionsAllowedIps' => array(
            '127.0.0.1',
            '203.191.201.182',        // Epic office
            '202.91.44.62',           // depo8
            '43.241.54.230',          // staging.yump.com.au (new server)
        ),

        // Card default settings
        'imageCardImagerDefaultSettings' => array(
            'width' => 1076,
            'ratio' => 16 / 9,
        ),
        'defaultCardCtaText' => 'Read more',
        'imageCardPlaceholderUrl' => false, // if path provided, we will use the placeholder image when card image is missing. The placeholder image's dimension should follow the 'imageCardImagerDefaultSettings'

        // cache
        'siteNavigationCacheKey' => 'site-navigation',
        'navShouldIncludeHomepage' => true, // if set to true, then the cached navigation content will include homepage at the front of the navItems list, when running the getNav() function under YumpModuleService. Please note, if we don't have homepage in the nav list, then the breadcrumb should include Home crumb; otherwise it shouldn't.
        'breadcrumbShouldIncludeHome' => true,

        // social media
        // define which social media links are shown. These URLs must be defined in SEOMatic under Social Media -> 'Same as Links' in order to display.
        'activeSocialMediaIcons' => array(
            array(
                'handle' => 'facebook',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'facebook-square',
            ),
            array(
                'handle' => 'twitter',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'twitter-square',
            ),
            array(
                'handle' => 'youtube',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'youtube-square',
            ),
            array(
                'handle' => 'instagram',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'instagram',
            ),
            array(
                'handle' => 'linkedin',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'linkedin',
            ),
        ),

        // Page IDs
        'newsLandingPageId' => 491,

        // search filters mapping (manually put in the IDs by checking the CMS)
        'searchFiltersMapping' => array(
            'pages' => array(
                'type' => 'entryType',
                'ids' => [2, 3, 4, 11, 20, 22, 16, 21],
            ),
            'news' => array(
                'type' => 'entryType',
                'ids' => [15],
            ),
            'stories' => array(
                'type' => 'entryType',
                'ids' => [18],
            ),
            'reports' => array(
                'type' => 'entryType',
                'ids' => [19],
            ),
            'factSheets' => array(
                'type' => 'entryType',
                'ids' => [23]
            )
            // 'documents' => array(
            //     'type' => 'assetVolume',
            //     'ids' => [3],
            // ),
        ),

        // what's the handle for the tags field, for entries and documents.
        'searchTagFieldHandle' => 'tags',
        'userSessionDuration' => 10800, //3 hours

        // samesite cookie value for security purposes. Craft's default is null, which means missing, but modern browsers default it to Lax anyway. We just explicitly set it to avoid questions from pen testers.
        // Ref: https://craftcms.com/docs/3.x/config/config-settings.html#samesitecookievalue
        'sameSiteCookieValue' => 'Lax',

        // For security reasons, set this to true to make enumerate users difficult. 
        // Ref: https://craftcms.com/docs/3.x/config/config-settings.html#preventuserenumeration
        'preventUserEnumeration' => true,

//        'defaultTokenDuration' => 86400, //1 days
        
    ],

    // Dev environment settings
    'dev' => [
        'enableLoginWallOnDocs' => false,
    ],

    // Staging environment settings
    'staging' => [
        'enableLoginWallOnDocs' => true,
    ],

    // Production environment settings
    'production' => [
        'enableLoginWallOnDocs' => true,
    ],
];
