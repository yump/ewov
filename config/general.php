<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // error page templates will be located under template root with '_' as prefix
        'errorTemplatePrefix' => "_errors/",

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // Whether Craft should allow system and plugin updates in the Control Panel, and plugin installation from the Plugin Store.
        'allowUpdates' => false,

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        //'useProjectConfigFile' => false,

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        //enable this to avoid a significant cache hit reduction for any static caching service is used
        'asyncCsrfInputs' => true,
        
        /**
         * Explicitly set the @web alias to avoid cache poisoning attach.
         * Remember to set DEFAULT_SITE_URL in your .env file (e.g. https://yump.com.au), without the slash at the end
         *
         * Ref:
         * - https://craftcms.com/knowledge-base/securing-craft#explicitly-set-the-web-alias-for-the-site
         * - https://craftcms.com/docs/3.x/config/#aliases
         */
        'aliases' => [
            '@web' => craft\helpers\App::env('DEFAULT_SITE_URL'),
        ],

        // login path
        'loginPath' => 'admin/login',

        // 'errorTemplatePrefix' => "_errors/",

        // 'defaultImageQuality' => 100,

        
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => true,
        'allowUpdates' => true,
        'testToEmailAddress' => array(
            'devs@yump.com.au' => 'Yump Devs'
        ),
    ],

    // Staging environment settings
    'staging' => [
        // Set this to `false` to prevent administrative changes from being made on staging
        'allowAdminChanges' => true,

        // Dev Mode (enabled when ?devMode=1 querystring is present)
        'devMode' => !empty($_GET['devMode']),
    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on production
        'allowAdminChanges' => true,
    ],
];
