<?php

return [
	'*' => [
		'suppressExceptions' => true,
		'allowUpscale' => false,
	],
];