if(!window._genesys.widgets.extensions){
    window._genesys.widgets.extensions = {};	
}
console.log(JSON.stringify(window._genesys.widgets));
console.log(JSON.stringify(window._genesys.widgets.extensions["CustomExtension"]));

var oMyPlugin = CXBus.registerPlugin('CustomExtension');
console.log("oMyPlugin Is" + JSON.stringify(oMyPlugin));

oMyPlugin.before('WebChatService.startChat', function (e) {

	var accountEmail = document.getElementById("cx_webchat_form_email");
    var accountEmailVal = accountEmail.value;
	
	var accountType = document.getElementById("cx_webchat_form_type");
    var accountTypeVal = accountType.value;
	
	var accountCase = document.getElementById("cx_webchat_form_case");
    var accountCaseVal = accountCase.value;
	
	var accountState = document.getElementById("cx_webchat_form_state");	
    var accountStateVal = accountState.value;
	
	var accountSupply = document.getElementById("cx_webchat_form_supply");
    var accountSupplyVal = accountSupply.value;
				
                var customDataAdditionalAttributes = {
                                "accountEmail": accountEmailVal,
								"accountType": accountTypeVal,
								"accountCase": accountCaseVal,
								"accountState": accountStateVal,
								"accountSupply": accountSupplyVal
                };
                oMyPlugin.command("WebChatService.setChatAdditionalAttributes", customDataAdditionalAttributes);
	console.log('Account Email is set as: ', accountEmailVal);
	console.log('Account Type is set as: ', accountTypeVal);
	console.log('Account Case is set as: ', accountCaseVal);
	console.log('Account State is set as: ', accountStateVal);
	console.log('Account Supply is set as: ', accountSupplyVal);
	
    return e;
});